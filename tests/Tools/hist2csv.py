import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import sys 
from mpl_toolkits.mplot3d import Axes3D
sys.path.append('C:/Users/up201/chem/myClasses/')
import myTime as T


if  __name__ == '__main__':
	n_s= 17
	n_c=3
	n_su = 40
	n_r = 2
	n_a = 60
	data = np.arange(n_s*n_c*n_su*n_r*n_a*1)
	data = data.reshape((n_s,n_c,n_su,n_r,n_a,1))
	clock = T.Clock(1)
	#print(data)
	print(data.shape)

	setups = np.arange(n_s)
	cameras = np.arange(n_c)
	subjects = np.arange(n_su)
	replications = np.arange(n_r)
	actions = np.arange(n_a)

	X= subjects
	Y = actions
	Z = setups

	SS = cameras*100
	cmap = data.reshape(-1,)
	Markers = ['x' if r == 0 else 'o' for r in replications]
	
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	n = 0
	clock.startTimer(0)
	for x in X:
		for y in Y:
			for z in Z:
				for mark in Markers:
					for s in SS:
						ax.scatter(x, y, z, alpha=0.4, cmap = 'hot', s=s, marker=mark)
						#print(x,y,z)
						n+=1
				#print('Z:',z)
			#print('Y:',y)
		print('n:',n, len(cmap))
	clock.stopTimer(0,'Total', verbose= True)
	plt.show()

		
		