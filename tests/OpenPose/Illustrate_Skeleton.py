import os
import numpy as np
import sys
sys.path.append('/Users/up201/chem')
import myLib as mL

from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
from matplotlib import cm
import cv2	
import sys
sys.path.append('C:/Users/up201/chem/myClasses/')
import JsonReader as JR
import tableHandler as TH

def updateFrame(i , coords, skel, fig, capt):
	print('\t\tUPDATE FRAME', i)
	
	if i<coords.shape[0]:
		ret, frame = capt.read()

		if ret == True:
			ym, xm, _ = frame.shape
			fx = xm
			fy = ym

			mL.plt.cla()										 #clean figure
			fig.suptitle(f'Frame_{i}', backgroundcolor = 'gray') #title of figure with background

			pos = coords[i,:30].reshape((15,2))					 # reshape to graph expected position shape

			mL.plt.xlim(0,xm)									 # set lims of ploted graph equals to lims of video source 
			mL.plt.ylim(ym*-1, 0)								 # in pixels
			
			size = fig.get_size_inches()*fig.dpi				 # get figure sixe in pixels

			frame= cv2.resize(frame, tuple(size.astype(int)) )   # resize cv to figure size
		
			mL.nx.draw(skel ,with_labels=True, pos = pos[:15,:], node_color='green', node_size=10) #draw skeleton
			
			ax=mL.plt.gca()
			ax.set_facecolor('gray')

			fig.canvas.draw()					#draw skel in canvas

	    	# convert canvas to image
			img2 = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8) 
			
			img2  = img2.reshape(fig.canvas.get_width_height()[::-1] + (3,))
			img2 = img2.copy()   # previous img2 is read only
		
			imgs = np.sum(img2, axis=2)				# Sum color channels
			img_i = np.argwhere(imgs == 255*3)		# Where white

			img = img2
			img[img_i[:,0], img_i[:,1],:]= frame[img_i[:,0], img_i[:,1],:] # put cv in back ground

			img = cv2.resize(img, ( fx, fy)) # set cv size

			cv2.imshow(f'Overlay', img) #show image

			if i >=34:		# enter key to start video
				
				print(pos)
				input()


def showSkel(data, videopath):
	

	Skel = mL.build_graph()		# build graph connections
	pos = data				# get positions

	pos[:, 0:30:2] *= 1			# Manipulate X
	pos[:, 1:30:2] *= -1		# Manipulate Y

	fig = mL.plt.figure()		# initiate figure

	a = fig.add_subplot(111)	# initiate axes
	a.set_position([0,0,1,1])	# stretch axes

	fig.set_facecolor('blue')
	a.set_facecolor('gray')

	cap = cv2.VideoCapture(videopath) # get video handler

	# Start video generation
	ani = FuncAnimation(mL.plt.gcf(), updateFrame, fargs = (pos, Skel, fig, cap),frames = pos.shape[0],interval = 10, repeat = False)

	# close Windows
	mL.plt.show()
	cap.release()
	cv2.destroyAllWindows()
	mL.plt.close()

def show_3dSkel(pos_table,video_Jlist, vname, ax2, pX=None, pZ=None, pY=None, colors=None):
	stored = pos_table[video_Jlist]['pos']

	stored_regroup = stored.reshape((-1,15,2))
	new_stored = np.zeros((len(stored), 15,3))
	new_stored[:,:,:2] = stored_regroup
	new_stored[:,:,2] = [ [n]*15  for n in range(len(new_stored)) ]
	new_stored = new_stored.reshape(-1,3)
	new_stored[:,1] = -1*new_stored[:,1] + 1080


	mycolors1 = cm.get_cmap('hot', len(stored))
	mycolors2 = mycolors1(np.linspace(0,1,len(stored)))
	mycolors3 = [[n]*15 for n in mycolors2]
	mycolors4 = np.array(mycolors3).reshape(-1,4)
	
	mycolors4[:,-1] = mycolors4[:,-1]/4

	#fig2 = plt.figure()

	

	#for coords in zip(pZ,pY,pZ):
	#	print(coords)

	ax2.scatter(new_stored[:,0], new_stored[:,2], new_stored[:,1], c = mycolors4)
	if (colors!=None) & (pX!=None) & (pY!=None) & (pZ!=None):
		pZ = list(range(len(pX)))
		ax2.scatter(pX, pZ, pY, color = colors )
	ax2.set_xlabel('$X$', fontsize=20, rotation=150)
	ax2.set_ylabel('$Time$')
	ax2.set_zlabel('$Y$', fontsize=30, rotation=60)
	ax2.set_xlim(0,1920)
	ax2.set_zlim(0,1080) 
	ax2.set_title(vname)
	#fig2.show()

if __name__ == '__main__':
	handler = TH.TableHandler()
	table = handler.tables.root.PosZeros

	tableraw = handler.tables.root.PosRaw

	for n in range(5, 13):
		vname = f'S002C002P003R002A{n+1:03}' #S001C001P002R001A003

		videoName = f'{vname}_rgb' #S001C001P002R001A002
		table_i = table.get_where_list(f'vname == \'{vname}\'')
		fig = plt.figure()
		ax = fig.add_subplot(121, projection='3d')
		show_3dSkel(tableraw, table_i, vname+' - Before', ax)
		ax = fig.add_subplot(122, projection='3d')
		show_3dSkel(table, table_i, vname+ ' - After', ax)
		plt.show()

		#videoName = f'S001C003P006R001A{n:03}_rgb'
		videopath = f'D:\\dataset\\nturgbd_rgb_s002\\nturgb+d_rgb\\{videoName}.avi'
		jsonFolder = f'C:\\Users\\up201\\Desktop\\Tese\\openposeOriginal\\openpose-master\\examples\\media\\json4\\{videoName[:-4]}_rgb\\'
		print(f'videopath: {videopath}')
		print(f'videopath: {jsonFolder}')

		#jr = JR.JsonReader('Action', jsonFolder, videopath)
		#jr.read()

		for n in table[table_i]:
			print('JNO: ', n['jno'])
			print(n['pos'])
			print('--'*50)

		showSkel(tableraw[table_i]['pos'], videopath)
		showSkel(table[table_i]['pos'], videopath)
