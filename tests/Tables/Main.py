import argparse
import ModeTypes as MT


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	subparsers = parser.add_subparsers(help = 'Functions')
	SB_parser = subparsers.add_parser('mode', help = 'Mode Operation' )

	SB_parser.add_argument("-m", "--mode", help = 'Mode to operate', choices = ['none_', 'nonesequence', 'noneend', 'twoper', 'zero', 'zeroending', 'move', 'zerofilter', 'nonefilter','testavgmove', 'test'], type = str.lower, required = True, nargs='*')
	SB_parser.add_argument('-w','--windowed', type=str.lower, choices = ['setup', 'camera','subject', 'replication', 'action'], help = 'Part by which it will be created windows')
	SB_parser.add_argument('-c', '--colored', type=str.lower, choices = ['setup', 'camera','subject', 'replication', 'action'], help = 'Part that will be represented by colors')
	SB_parser.add_argument('-X','--XX', type=str.lower, choices = ['setup', 'camera','subject', 'replication', 'action'], help = 'Part in the X axis of each graph')
	#parser.add_argument("-m", "--mode", help = 'Test Mode', choices = ['stackedbars', 'none'], type = str.lower, required = True)
	args = None
	try:
		args = parser.parse_args()
		if args.mode:
			print(f'Selected Mode: {args.mode}')
			arg_dict = vars(args).copy()
			arg_dict = {k: v for k,v in arg_dict.items() if v is not None}
			arg_dict.pop('mode')
			function = str(args.mode[0]) + 'statistics'
			ret = getattr(MT,function)(**arg_dict)

	except SystemExit:
		pass	


	