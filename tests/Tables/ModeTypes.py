import sys
import numpy as np
import matplotlib.pyplot as plt
sys.path.append('C:/Users/up201/chem/myClasses/')
import tableHandler as tH 
import None_Checker as NC
import Plotter as Pl
import myTime as T
import Bar as B
import pandas as pd 
import jenkspy
import matplotlib.patches as mpatches
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


def parseVideoName(name, verbose = 0):
	# Input : Video_name  Verbose
	# Output: Tupe(Setup, Camera, Subject, Replication, Action, Rest of the name as String )
	s = int(name[1:4])
	c= int(name[5:8])
	su= int(name[9:12])
	r= int(name[13:16])
	a= int(name[17:20])
	rest = name[20:]

	if verbose:
		print(f'Name: {name}\tSet: {s}, Cam: {c}, Sub: {su}, Rep: {r}, Act: {a}, Rest: {rest} ')
		input()
	return s,c,su,r,a, rest

def none_statistics(windowed = 'setup', colored='subject', XX ='action'):
	modes = [{'colored' : None},
			{'windowed' : 'setup','colored' : 'subject', 'XX': 'action'},
			{'windowed' : 'setup','colored' : 'action', 'XX': 'subject'},
			#{'windowed' : 'subject','colored' : 'setup', 'XX': 'action'},
			{'windowed' : 'subject','colored' : 'action', 'XX': 'setup'},
			{'windowed' : 'action','colored' : 'subject', 'XX': 'setup'},
			#{'windowed' : 'action','colored' : 'setup', 'XX': 'subject'},
			]
	fields = ['setup', 'subject', 'action']
	clock = T.Clock(2)
	clock.startTimer(0)
	clock.startTimer(1)
	ploter = Pl.Plotter()
	#for f in fields:
		#ploter.plot(f)


	for kwargs in modes:
		print(kwargs)
		nonestatistics(**kwargs)
		clock.startTimer(clock.stopTimer(1,f'None statistics for: {kwargs} Duration', verbose = True))
	clock.stopTimer(0,f'None statistics Full Duration', verbose = True)

def nonestatistics(windowed= 'setup', colored=None, XX ='action', preConditions = [], condition = None, test = 61, pf ='StackedBars', midRetA = True): #NoneStatistics
	if condition:
		ploter = Pl.Plotter(condition)
	else:
		ploter = Pl.Plotter()
	clock=T.Clock(1)
	same_ylim = 0
	if not colored:
		pf = 'Bars'
	same_ylim = 1

	clock.startTimer
	ploter.subplot( windowed = windowed, colored = colored, XX= XX, preConditions= preConditions , test= test , midRetA = midRetA, ylim = same_ylim)
	clock.stopTimer(0)


def AddMiniCond(checker, field,condition):
	expected = checker.countCol(field)[:,0].astype(int)
	cond = f'({field} == {expected[0]})'
	for n in expected[1:]:
		cond = NC.AddCondition(cond, f'{field} == {n}', '|')
	condition =NC.AddCondition(condition, cond, '&')
	return condition, expected



def nonesequencestatistics(): #NoneStatistics
	
	ploter = Pl.Plotter()
	checker = NC.None_Checker()
	meta_table = checker.meta_table
	maps_i= []
	jnos = []
	vnames = []
	end_counter = 0
	clock = T.Clock(1)

	clock.startTimer(0)
	data= checker.getNoneVideoLengs(False)
	clock.stopTimer(0)


	for n in range(data.shape[0]):
		condition = f'(pco == 0) & (setup == {data[n,0]}) & (camera == {data[n,1]}) & (subject == {data[n,2]}) & (replication == {data[n,3]}) & (action == {data[n,4]})'
		condition_last = f'(pco == 0) & (jno == {data[n,5]-1}) & (setup == {data[n,0]}) & (camera == {data[n,1]}) & (subject == {data[n,2]}) & (replication == {data[n,3]}) & (action == {data[n,4]})'
		end_counter += len(meta_table.get_where_list(condition_last)) 
		#print(n,condition_last,'\t', end_counter)
		maps_i += [meta_table.get_where_list(condition).copy()]

	#print(maps_i)
	for i,line in enumerate(maps_i):
		meta_line = meta_table[line[0]]
		
		s= meta_line['setup']
		c= meta_line['camera']
		su= meta_line['subject']
		r= meta_line['replication']
		a= meta_line['action']

		jnos += [meta_table[line]['jno'] ]

		
		vname = f'S{s:03}C{c:03}P{su:03}R{r:03}A{a:03}'
		vnames += [vname]
		
	ploter.plotHeatmap([vnames, jnos, data[:,5],False], )
	plt.show()
	return maps_i

def noneendstatistics():
	def getEndingSequence(meta_table,endJ, condition, dmin = -1):
		js = meta_table.col('jno')
		ret = []
		stop  = False
		for n in range(endJ, dmin, -1):
			if not stop:
				leng = len(meta_table.get_where_list(condition + f' & (jno == {js[n]})'))
				#print(f'leng == {leng}',endJ, n, dmin, condition + f' & (jno == {js[n]})') 
				if leng:
					ret += [meta_table[n]['jno']]
				else:
					
					stop = True
			else:
				n = -1
		return ret


	clock = T.Clock(1)
	ploter = Pl.Plotter()
	checker = NC.None_Checker()
	meta_table = checker.meta_table

	ending_jlist = []
	ending_videoName = []
	
	data= checker.getNoneVideoLengs(verbose = False)
	
	clock.startTimer(0)
	lengs_i = []


	save = False
	for n in range(data.shape[0]):
		
		s= data[n,0]
		c= data[n,1]
		su= data[n,2]
		r= data[n,3]
		a= data[n,4]

		normalCondition =f'(pco == 0) & (setup == {s}) & (camera == {c}) & (subject == {su}) & (replication == {r}) & (action == {a})' 
		condition_last = normalCondition + f' & (jno == {data[n,5]-1})'
		condition_init = f'(jno == 0) & (setup == {s}) & (camera == {c}) & (subject == {su}) & (replication == {r}) & (action == {a})'
		
		endJsons_i = meta_table.get_where_list(condition_last)
		initJson_i = meta_table.get_where_list(condition_init)
		
		line = []
		
		for i,(n_i, dmin_i) in enumerate(zip(endJsons_i, initJson_i)):

			newElement =  getEndingSequence(meta_table, n_i, normalCondition, dmin_i) 			
			line += newElement
		
		if len(line):
			save = True
			ending_jlist += [line]

		if save: 
			lengs_i += np.where((data[:,0] == s) & (data[:,1] == c) & (data[:,2] == su) & (data[:,3] == r) & (data[:,4] == a))[0].tolist()
			vname = f'S{s:03}C{c:03}P{su:03}R{r:03}A{a:03}'
			ending_videoName += [vname]
		save = False

	print('Lens: \t',len(ending_videoName), len(ending_jlist), len(lengs_i))
	print(ending_videoName)
	print(ending_jlist)
	ax=ploter.plotHeatmap([ending_videoName, ending_jlist, data[lengs_i,5],False], )
	ax.set(xlabel='Jsons__')
	
	plt.show()

def twoperstatistics():
	#nonestatistics(windowed = 'setup',colored='subject', XX='action',preConditions = ['action < 40 '], condition = 'pco > 1', pf='StackedBars', midRetA = False, test = 1)
	modes = [{'windowed' : 'setup','colored' : 'subject', 'XX': 'action'},
			{'windowed' : 'setup','colored' : 'action', 'XX': 'subject'},
			{'windowed' : 'subject','colored' : 'setup', 'XX': 'action'},
			{'windowed' : 'subject','colored' : 'action', 'XX': 'setup'},
			{'windowed' : 'action','colored' : 'subject', 'XX': 'setup'},
			{'windowed' : 'action','colored' : 'setup', 'XX': 'subject'},
			]
	clock= T.Clock(len(modes)+1)
	clock.startTimer(len(modes))

	for timerId,kwargs in enumerate(modes):
		print(f'\n\n-------------------------------------------------------------------------------------------------------------------------\n\t\t\tStart {kwargs}\n\n')
		clock.startTimer(timerId)
		try:
			nonestatistics(**kwargs,preConditions = ['action < 40 '], condition = 'pco > 1',  midRetA = False, test = 600)
		except KeyboardInterrupt:
			print('\n\tINTERRUPTED !!!')
			clock.stopTimer(timerId,f'\n\t{kwargs} Ended after',verbose = True)
			sys.exit(0)

		clock.stopTimer(timerId, f'{kwargs}',verbose = True)
	clock.stopTimer(len(modes),'Total',verbose= True)

	for timer in clock.timers:
		print(timer.id, timer._dur)
	return 1

def testavgmovestatistics(updateMove=False, updateAvg = False):

	handler = tH.TableHandler()
	A = 1
	if updateAvg:
		updateMove=True
		if handler.tables.root.average.__contains__(f'pos'):
			handler.tables.remove_node(handler.tables.root.average.pos)
		handler.create_Average()
		handler.build_Average()
	
	if updateMove:
		if handler.tables.root.__contains__(f'avgMove'):
			handler.tables.remove_node(handler.tables.root.avgMove)
		handler.create_Move()
		handler.build_Move()

	aM= handler.tables.root.avgMove
	mm = aM.cols.move_max

	
	avg, std = mm[:].mean(), mm[:].std()
	
	markedVideos = aM.get_where_list(f'(action >=40) & (action < 50)')#(move_max > {avg+A*std} ) & 

	with open(f'Artifacts/marked_2D_{A}std_A4050.txt', 'a') as f: 
		field = 'vName'
		decoder = 'utf-8'
		for mvideo in markedVideos:
			f.write(f'{aM[mvideo][field].decode(decoder)}\n') 




def _getZerosIndex(table, field = 'pos', verbose =False):
	clock = T.Clock(2)
	row_i = []
	cols_i =[]
	clock.startTimer(0)
	clock.startTimer(1)
	nmax=0
	for i,row in enumerate(table.iterrows()):
		name = row['vname']
		_,_,_,_,a,_ = parseVideoName(name)
		if a<50:
			if np.any(row['pos'] == 0):
				ind = np.where(row['pos']==0)[0]+1
				if len(ind) == 28:
					nmax += 1
				cols_i +=[ind.astype(int)]
				row_i += [i]
			if verbose:
				if (i+1) % 100000==0:
					message = f'{i+1} / {len(table)}\t Len = {len(row_i)} Duration:'
					clock.startTimer(clock.stopTimer(1,message, verbose = verbose))

	clock.stopTimer(0,'Total', verbose = verbose)
	#print(nmax)
	#input()
	return np.array(row_i).astype(int), cols_i


def zeroendingstatistics():

	modes = [ 'setup', 'camera', 'subject', 'replication', 'action'  ]
	subPosition=[ 	['341', '342'], ['343', '344'],
					['345', '346'], ['347', '348'],
					['325', '326']   ]
	colors = ['red', 'blue', 'green', 'orange', 'purple']

	clock = T.Clock(2)
	th = tH.TableHandler()

	meta_table = th.tables.root.Metadata
	pos_table = th.tables.root.PosProcessed
	zeros_table = th.tables.root.PosZeros
	lengs_table = th.tables.root.VideoLengths

	row_i = []
	cols_i =[]

	clock.startTimer(0)

	try:
		clock.startTimer(1)
		row_i, cols_i =	_getZerosIndex(pos_table, field = 'pos', verbose = True)
		clock.startTimer(clock.stopTimer(1, 'Finished getting Zero Indexes',verbose =True))
		
		for sPos, field, graphColor in zip(subPosition,modes, colors):



			hist= np.zeros((B.CONSTANT[f'{field}'],2), dtype=int)
			ratios = np.zeros((B.CONSTANT[f'{field}'],2), dtype=float)
			
			clock.startTimer(2)
			histTable = th.create_Histogram('zeros', f'zeroEnding_{field.capitalize()}s_hist')
			ratioTable = th.create_Histogram('zeros', f'zeroEnding_{field.capitalize()}s_ratio')
			print(histTable.shape, ratioTable.shape, ' - ', hist.shape, ratios.shape)
			for n_field in range(B.CONSTANT[f'{field}']):
				if n_field>= len(histTable):
					print('\n'+ '---'*30)
					print('-'*15, f'Starting {field} {n_field + 1}', '-'*15)
					hist[n_field,0]=n_field+1
					ratios[n_field,0]=n_field+1

					field_i =meta_table.get_where_list(f'({field} == {n_field+1})')
					intersect = np.intersect1d(field_i, row_i)

					clock.startTimer(clock.stopTimer(2, f'Finished Intersection Between {field.capitalize()} == {n_field+1} and Pos has a zero',verbose =True))
					
					s,c,su,r,a = meta_table.col('setup')[intersect].reshape((-1,1)), meta_table.col('camera')[intersect].reshape((-1,1)), meta_table.col('subject')[intersect].reshape((-1,1)), meta_table.col('replication')[intersect].reshape((-1,1)), meta_table.col('action')[intersect].reshape((-1,1))
					cond_data = np.hstack((s,c,su,r,a))
					cond_data = np.unique(cond_data, axis = 0)
					
					ending = []
					lengs = []
					clock.startTimer(clock.stopTimer(2, f'Finished Getting Videos in Last Intersect',verbose =True))
					clock.startTimer(3)
					for video_i in range(cond_data.shape[0]):

						newcondition =  f'(setup == {cond_data[video_i,0]}) & (camera == {cond_data[video_i,1]}) & (subject == {cond_data[video_i,2]}) & (replication == {cond_data[video_i,3]}) & (action == {cond_data[video_i,4]})'
						leng = lengs_table.read_where(newcondition, field = 'length')[0]
						lengs += [leng]
						#print(newcondition, leng, meta_table.col('jno')[matches[-1]], pos_table.col('vname')[matches[-1]])
						if leng>0:
							ret = meta_table.get_where_list(newcondition + f' & (jno == {leng-1})')
							ending += [ret[0]]
						#	print('\t',newcondition + f' & (jno == {leng-1})' , ending, type(ret))
						if (video_i+1) %100 == 0:
							clock.startTimer(clock.stopTimer(3, f'{video_i+1} / {cond_data.shape[0]}\tFinished Get Ending for {pos_table.cols.vname[ending[video_i]]}',verbose =True))

					ending = np.array(ending, dtype= int)
					clock.startTimer(clock.stopTimer(2, f'Finished Getting Ending Indexes',verbose =True))
					
					#print('Before: ',ending.shape, intersect.shape)
					intersect = np.intersect1d(intersect, ending)
					#print('After: ',intersect.shape)
					clock.startTimer(clock.stopTimer(2, f'Finished Intersection Between previous Intersection and jno is the last jno of video',verbose =True))
					hist[n_field,1]=intersect.shape[0]

					print(f'ratio: {intersect.shape} {field_i.shape} = {intersect.shape[0]/field_i.shape[0]}')
					ratios[n_field,1] = (intersect.shape[0]/len(lengs_table)) * 100

					th.buildHistogram(histTable,hist[n_field,:].reshape(1,-1), xlabels =f'{field} {n_field+1}'  )
					th.buildHistogram(ratioTable,ratios[n_field,:].reshape(1,-1), xlabels =f'{field} {n_field+1}')
					#print(field_i.shape, row_i.shape, hist[n_field,:], ratios[n_field,:])
			
			np.save(f'Artifacts/{field.capitalize()}_hist',hist)
			np.save(f'Artifacts/{field.capitalize()}_ratios',ratios)
			plt.subplot(sPos[0])
			hm = B.Bars(title=f'Number of Zeros per {field.capitalize()}')
			hm.plot(hist, show = False, color = graphColor)
			#	plt.gca().set_xticks(plt.gca().get_xticks()-0.5)

			plt.subplot(sPos[1])
			hm = B.Bars(title=f'Percentage of Zeros per {field.capitalize()}')
			hm.plot(ratios, show = False, color = graphColor)

			clock.startTimer(clock.stopTimer(1, f'Finished Processing Graph {field.capitalize()}',verbose =True))

		plt.subplots_adjust(hspace = 0.6, wspace = 0.3)
		plt.show()


	except KeyboardInterrupt:
		print('\n\tINTERRUPTED !!!')
		clock.stopTimer(0, 'Timer interrupted after:', verbose = True)
		sys.exit(0)

	clock.stopTimer(0, 'Total', verbose = True)

	return 1

def zerostatistics():

	modes = [ 'setup', 'camera', 'subject', 'replication', 'action'  ]
	subPosition=[ 	['341', '342'], ['343', '344'],
					['345', '346'], ['347', '348'],
					['325', '326']   ]
	colors = ['red', 'blue', 'green', 'orange', 'purple']

	clock = T.Clock(2)
	th = tH.TableHandler()

	meta_table = th.tables.root.Metadata
	#pos_table = th.tables.root.PosProcessed
	zeros_table = th.tables.root.PosZeros
	lengs_table = th.tables.root.VideoLengths
	pos_table = zeros_table
	row_i = []
	cols_i =[]

	clock.startTimer(0)

	try:
		clock.startTimer(1)
		row_i, cols_i =	_getZerosIndex(pos_table, field = 'pos', verbose = True)
		clock.startTimer(clock.stopTimer(1, f'Finished getting Zero Indexes {len(row_i)}',verbose =True))
		
		input()
		for sPos, field, graphColor in zip(subPosition,modes, colors):



			hist= np.zeros((B.CONSTANT[f'{field}'],2), dtype=int)
			ratios = np.zeros((B.CONSTANT[f'{field}'],2), dtype=float)
			
			clock.startTimer(2)
			histTable = th.create_Histogram('zeros', f'zero_New3_{field.capitalize()}s_hist')
			ratioTable = th.create_Histogram('zeros', f'zero_New3_{field.capitalize()}s_ratio')
			print(histTable.shape, ratioTable.shape, ' - ', hist.shape, ratios.shape)
			for n_field in range(B.CONSTANT[f'{field}']):
				if n_field>= len(histTable):
					print('\n'+ '---'*30)
					print('-'*15, f'Starting {field} {n_field + 1}', '-'*15)
					hist[n_field,0]=n_field+1
					ratios[n_field,0]=n_field+1

					field_i =meta_table.get_where_list(f'({field} == {n_field+1})')
					intersect = np.intersect1d(field_i, row_i)

					clock.startTimer(clock.stopTimer(2, f'Finished Intersection Between {field.capitalize()} == {n_field+1} and Pos has a zero',verbose =True))
					
					
					lengs = []
					clock.startTimer(clock.stopTimer(2, f'Finished Getting Videos in Last Intersect',verbose =True))
					
					
					hist[n_field,1]=intersect.shape[0]

					#print(f'ratio: {intersect.shape} {field_i.shape} = {intersect.shape[0]/field_i.shape[0]}')
					ratios[n_field,1] = (intersect.shape[0]/len(lengs_table)) * 100

					th.buildHistogram(histTable,hist[n_field,:].reshape(1,-1), xlabels =f'{field} {n_field+1}'  )
					th.buildHistogram(ratioTable,ratios[n_field,:].reshape(1,-1), xlabels =f'{field} {n_field+1}')

			plt.subplot(sPos[0])
			hm = B.Bars(title=f'Number of Zeros per {field.capitalize()}')
			hm.plot(hist, show = False, color = graphColor)
			#	plt.gca().set_xticks(plt.gca().get_xticks()-0.5)

			plt.subplot(sPos[1])
			hm = B.Bars(title=f'Percentage of Zeros per {field.capitalize()}')
			hm.plot(ratios, show = False, color = graphColor)

			clock.startTimer(clock.stopTimer(1, f'Finished Processing Graph {field.capitalize()}',verbose =True))

		plt.subplots_adjust(hspace = 0.6, wspace = 0.3)
		plt.show()


	except KeyboardInterrupt:
		print('\n\tINTERRUPTED !!!')
		clock.stopTimer(0, 'Timer interrupted after:', verbose = True)
		sys.exit(0)

	clock.stopTimer(0, 'Total', verbose = True)

	return 1

def buildmovestatistics():

	handler = tH.TableHandler()
	pos_table = handler.tables.root.PosRaw
	average_table = handler.tables.root.average.pos
	clock= T.Clock(2)


	weights = np.ones((30,))
	weights[1], weights[2], weights[5], weights[8], weights[9], weights[12] = 3,2,2,3,2,2
	i=0
	leng = len(pos_table)

	clock.startTimer(0)
	clock.startTimer(1)
	for row in pos_table.iterrows():
		a = row['pos']
		avg = np.average(a, axis = 0, weights = weights)
		
		new_row = (row['vname'].decode('utf-8'), row['jno'], avg)
		
		#print(new_row)
		#print(average_table)

		average_table.append([new_row])	

		average_table.flush()
		
		i+=1
		if (i)% 100000 == 0:
			clock.startTimer(clock.stopTimer(1, f'{i+1} / {leng}', verbose = True))


	clock.stopTimer(0,'Full Durations:', verbose  =True)

def zerofilterstatistics():
	handler = tH.TableHandler()
	pos_table = handler.tables.root.PosZeros
	leng_table = handler.tables.root.VideoLengths
	clock = T.Clock(2)	

	clock.startTimer(0)
	rows_i, cols_i = _getZerosIndex(pos_table, verbose = True)
	clock.stopTimer(0, 'Finished Getting Zero Indexes', verbose = True)
	vnames = pos_table[rows_i]['vname'] 

	vnames_u = np.unique(vnames)


	print(len(vnames_u))

	ret = []
	leng_map = []

	clock.startTimer(0)
	
	clock.startTimer(1)
	for i,uni in enumerate(vnames_u):
		ret+=[np.where(vnames == uni)]
		leng_map += [leng_table.get_where_list(f'vname == {uni}')]
		#print(ret)
		#input()

		if (i+1)%1000 == 0:
			clock.startTimer(clock.stopTimer(1,f'{i+1}/{len(vnames_u)}', verbose = True))

	clock.stopTimer(0, 'Finished Getting Video with Zero Indexes', verbose = True)

	print(len(ret))

	clock.startTimer(0)
	clock.startTimer(1)

	#for every video with jsons with 0
	for m_i, video_Jlist in enumerate(ret):

		found_zero=0
		
		#for every json with 0
		for J in video_Jlist[0]:

			#if not first
			if (pos_table[rows_i[J]]['jno']>0):
				data = pos_table[rows_i[J]-1]['pos']

				#for every 0
				for col in cols_i[J]:

					#if have previous data
					col= col-1
					if data[col] !=0:
						ini = rows_i[J]-1
						leng = leng_table[ leng_map[m_i] ][ 'begining' ]+leng_table[ leng_map[m_i] ][ 'length' ][0]
						fin = leng_table[ leng_map[m_i] ][ 'begining' ]+leng_table[ leng_map[m_i] ][ 'length' ][0]

						nj = rows_i[J]
						while nj < leng:
							if(pos_table[nj]['pos'][col] == 0):

								nj +=1
							else:
								fin = nj
								nj = leng

						if fin != leng:

							f_ini = pos_table[ini]['pos'][col]
							f_fin = pos_table[fin]['pos'][col]
							dt = fin-ini

							m = (f_fin - f_ini) / dt

							new_rows = []
							new_r = []

							for n in range(1,dt):
								#print('dt:', dt, 'n:', n)
								new_value = f_ini + m * n
								new_value_row, new_value_col = ini+n, col
								
								#print('Previous Row:', pos_table[new_value_row]['pos'])
								new_ = pos_table[new_value_row]
								new_['pos'][new_value_col] = new_value

								pos_table[new_value_row] = [new_]
								
								new_rows += [new_]
								new_r += [new_value_row]

							pos_table.flush()

		if (m_i+1) % 1000 == 0:
			clock.startTimer(clock.stopTimer(1,f'Video {m_i}/{len(ret)}', verbose = True))

	clock.stopTimer(0, 'Finished Modifying Rows', verbose = True)

def nonefilterstatistics():
	handler = tH.TableHandler()
	pos_table = handler.tables.root.PosZeros
	meta_table = handler.tables.root.Metadata
	leng_table = handler.tables.root.VideoLengths
	clock = T.Clock(2)	

	clock.startTimer(0)
	
	cand = meta_table.get_where_list('action <50')
	myList= np.where(np.isnan(pos_table[cand]['pos']))[0]

	clock.stopTimer(0, 'Finished Getting Indexes', verbose = True)
	
	print(len(myList))
	print(pos_table[cand[myList[0]]])




def show_3dSkel(pos_table,video_Jlist,pX, pZ, pY, colors, vname, ax2):

	stored = pos_table[video_Jlist]['pos']

	stored_regroup = stored.reshape((-1,15,2))
	new_stored = np.zeros((len(stored), 15,3))
	new_stored[:,:,:2] = stored_regroup
	new_stored[:,:,2] = [ [n]*15  for n in range(len(new_stored)) ]
	new_stored = new_stored.reshape(-1,3)
	new_stored[:,1] = -1*new_stored[:,1] + 1080


	mycolors1 = cm.get_cmap('hot', len(stored))
	mycolors2 = mycolors1(np.linspace(0,1,len(stored)))
	mycolors3 = [[n]*15 for n in mycolors2]
	mycolors4 = np.array(mycolors3).reshape(-1,4)
	
	mycolors4[:,-1] = mycolors4[:,-1]/4

	#fig2 = plt.figure()

	
	pZ = list(range(len(pX)))

	#for coords in zip(pZ,pY,pZ):
	#	print(coords)

	ax2.scatter(new_stored[:,0], new_stored[:,2], new_stored[:,1], c = mycolors4)
	if colors & pX & pY & pZ:
		ax2.scatter(pX, pZ, pY, color = colors )
	ax2.set_xlabel('$X$', fontsize=20, rotation=150)
	ax2.set_ylabel('$Time$')
	ax2.set_zlabel('$Y$', fontsize=30, rotation=60)
	ax2.set_xlim(0,1920)
	ax2.set_zlim(0,1080) 
	ax2.set_title(vname)
	#fig2.show()

def show_clusterCenters(kx,ky,kz,kc, ax):
	ax.scatter(kx, kz, ky,s = 10, c = kc) #list( sc_color(np.linspace( 0,1,len(YY) )) ))	

	#ax.set_facecolor('lightgray')
	ax.set_xlim(0,1920)
	ax.set_zlim(0,1080)
	ax.set_title('Cluster Centers Distribution')
	ax.set_xlabel('$X$', fontsize=20, rotation=150)
	ax.set_ylabel('$Time$')
	ax.set_zlabel('$Y$', fontsize=30, rotation=60)
	plt.show()

def teststatistics():
	A=1
	handler = tH.TableHandler()
	clock = T.Clock(2)
	pos_table = handler.tables.root.PosProcessed
	raw_table = handler.tables.root.PosRaw
	average_table = handler.tables.root.average.pos

	with open(f'Artifacts/marked_2D_{A}std_prev.txt', 'r') as f:
		data = f.readlines()
		words = [line.split()[0] for line in data]
	a = np.zeros((len(words),))
	print(words[12])
	trackers = []
	tracker = {
		'pno' : '',
		'jno' : [],
		'vname' : ''
	}
	clock.startTimer(0)
	clock.startTimer(1)
	kmeans = KMeans(n_clusters=2, random_state=0)
	XX = []
	YY = []
	ZZ = []
	CC = []
	ClCe = []
	fig = plt.figure()
	ax = fig.add_subplot(235, projection='3d')
	sc_color = cm.get_cmap('hot', len(words))
	for v_i, vname in enumerate(words):
		if v_i > -10 :

			video_Jlist = average_table.get_where_list(f'vName == \'{vname}\' ')

			pX = average_table[video_Jlist]['x_average']
			pY = average_table[video_Jlist]['y_average']
			pZ= [v_i]*len(pX) 


			XX += pX.tolist()
			YY += (-1 * pY + 1080).tolist()
			ZZ += pZ
			
			points = np.zeros((pX.shape[0],2))

			for i,(x,y) in enumerate(zip(pX,pY)):
				points[i,:] = [x,y]


			kmeans.fit(points)
			Xs = np.absolute(kmeans.cluster_centers_[:,0]- (1920/2)).argmin()
			
			colors = ['blue' if n != Xs else 'green' for n in kmeans.labels_]
			CC+= [np.where(np.array(colors) != 'green')[0].tolist()]
			ClCe += [tuple(kmeans.cluster_centers_[Xs,:])]

			if (v_i) % 60==0:
				if True:
					ax2 = fig.add_subplot(221, projection='3d')
					show_3dSkel(raw_table, video_Jlist, pX, pZ, pY, colors, vname+' - Before', ax2)
					ax2 = fig.add_subplot(222, projection='3d')
					show_3dSkel(pos_table, video_Jlist, vname + ' - After', ax2, pX= pX, pY=pY, pZ = pZ, colors = colors)

				clock.startTimer(clock.stopTimer(1,f'{v_i+1} / {len(words)}  --  {len(ClCe)}\tDuration:', verbose =True))
			

			kx, ky = kmeans.cluster_centers_[:,0], kmeans.cluster_centers_[:,1]
			kz = [v_i]* len(kx)
			kc = ['red']*2
			kc[Xs] = 'green'
			
			show_clusterCenters(kx,ky,kz,kc, ax)
			plt.show()
			if (v_i) % 60==0:
				input()

	#clock.stopTimer(0, 'total', verbose=True)
	line_counter = 0
	with open(f'Artifacts/rows_2D_update_A4050.txt', 'a') as f:
		for vname, avg_pos,j2u in zip(words, ClCe, CC):

			line = f'{vname} {avg_pos} {j2u}\n'
			#print(line)
			line_counter += 1 
			#f.write(line)
	clock.stopTimer(0, f'{line_counter} lines written - Total', verbose=True)

def none():
	print('No Mode Type')

if __name__ == '__main__':
	handler = tH.TableHandler()
	pos_table = handler.tables.root.PosProcessed
	zero_table = handler.tables.root.PosZeros
	pz = _getZerosIndex(zero_table, verbose = False)
	pp = _getZerosIndex(pos_table, verbose = False)
	for row_p, row_z in zip(pp[0], pz[0]):
		print(pos_table[row_p]['vname'])
		print(zero_table[row_z]['vname'])
		input()
