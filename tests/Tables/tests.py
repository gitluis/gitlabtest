import numpy as np 
import sys
sys.path.append('C:/Users/up201/chem/myClasses/')
import Verifier
import tableHandler as tH 


if __name__ == '__main__':
	th = tH.TableHandler()
	meta_table = th.tables.root.Metadata
	gVer = Verifier.GeneralVerifier(meta_table)
	gVer.print()

	sVer = Verifier.SetupVerifier(meta_table)
	sVer.print()