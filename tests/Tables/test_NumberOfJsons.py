import numpy as np
import sys
sys.path.append('C:/Users/up201/chem/Classes/')
import NumberOfJsons as NJ 
import tableHandler as tH 

if __name__ == '__main__':
	th = tH.TableHandler()
	nj = NJ.NumberOfJsons()

	tableRows = len(th.tables.root.Pos)
	n_jnos = np.sum(nj.videos_json)

	print('/'*20)
	print(f'\n\tTable Rows: {tableRows} | Number Of Jsons: {n_jnos}')
	if tableRows == n_jnos:
		print('\t\tTest Passed!\n')
	else:
		print('\t\tTest Reproved!\n')
	print('/'*20)

