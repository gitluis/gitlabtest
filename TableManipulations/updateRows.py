import numpy as np 
import numpy.linalg as linalg
import sys
sys.path.append('C:/Users/up201/chem/myClasses/')

import tableHandler as TH
import JsonReader as JR
import myTime as T


class Updater():
	def __init__(self, table_loc = '/Tables/dataset.h5', table_name = 'Pos' ):
		self.handler = TH.TableHandler(path = table_loc)
		self.table = self.handler.tables.root._f_get_child(table_name)
		self.lengTable = self.handler.tables.root.VideoLengths
	
	def updateRows(self, row_vname = '', row_jnos = [], row_avg=(),source = None, verbose=False):
		clock = T.Clock(5)

		if verbose:
			print('vVv'*50)
			print(f'\nName: {row_vname}\nJnos: {row_jnos}\nSource: {source}\n')
			print('row_avg: ', row_avg)
		clock.startTimer(0)
		clock.startTimer(1)

		read_dur = 0
		r1_dur = 0
		r2_dur = 0
		write_dur = 0
		for i, jno in enumerate(row_jnos):
			clock.startTimer(2)
			jno_string = 'jno'
			#table_i = self.table.get_where_list(f'(vname == \'{row_vname}\') & (jno == {jno})' )

			table_i2 = self.lengTable.get_where_list(f'vname == \'{row_vname}\'')
			table_i = self.lengTable[table_i2]['begining'] + jno
			
			jr = JR.JsonReader('', source)

			jr.read([jno], avg = row_avg)

			clock.stopTimer(2)
			read_dur += clock.timers[2]._dur


			clock.startTimer(2)

			row =self.table[table_i[0]]	
			#print(f'Old: {self.table[table_i[0]]}')
			row[0] = jr.data[jno,:30]
			self.table[table_i[0]] = [row]
			#print(f'New: {self.table[table_i[0]]}')
			
			clock.stopTimer(2)
			write_dur += clock.timers[2]._dur

			if (i+1)%10 == 0:
				if verbose:
					print(f'\tRead - Write percentage: {100*read_dur/(write_dur+read_dur):.2f}  -  {100*write_dur/(write_dur+read_dur):.2f}')
				clock.startTimer(clock.stopTimer(1, f'Update {i+1} of {len(row_jnos)} jsons\tDuration', verbose = verbose))
				if verbose:
					print('\n', '---'*50, '\n')



		with open(f'Artifacts/rowsUpdated_A4050.txt', 'a') as f:

			line = f'{row_vname} {row_jnos}\n'
			#print('line: ',line)
			f.write(line)
		clock.stopTimer(0, 'Updating Rows Duration', verbose = verbose)
		if verbose:
			print('^^^'*50)

