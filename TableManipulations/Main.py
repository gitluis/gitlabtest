import numpy as np 
import os
import math
import updateRows as uR
import sys
sys.path.append('C:/Users/up201/chem/myClasses/')
import myTime as T
import tableHandler as TH
import JsonReader as JR

import sys
sys.path.append('myClasses/TableDescriptions')
import Pos as POS

MIDDIR = 'nturgb+d_rgb'
PREF_DIR = 'nturgbd_rgb_s'

def parseVideoName(name, verbose = 0):
	# Input : Video_name  Verbose
	# Output: Tupe(Setup, Camera, Subject, Replication, Action, Rest of the name as String )
	s = int(name[1:4])
	c= int(name[5:8])
	su= int(name[9:12])
	r= int(name[13:16])
	a= int(name[17:20])
	rest = name[20:]

	if verbose:
		print(f'Name: {name}\tSet: {s}, Cam: {c}, Sub: {su}, Rep: {r}, Act: {a}, Rest: {rest} ')
		input()
	return s,c,su,r,a, rest


def buildDataset():
	jsonDirDir = 'C:/Users/up201/Desktop/Tese/openposeOriginal/openpose-master/examples/media/json4/'
	videosDirDir = 'D:/dataset/'
	tableName = 'PosRaw'

	dataset = TH.TableHandler()
	file = dataset.tables
	if not file.root.__contains__(tableName):
		file.create_table(file.root, tableName, description=POS.Sample)
		table = dataset.tables.root._f_get_child(tableName)
	else:
		table = dataset.tables.root._f_get_child(tableName)
		print(f'Remove {len(table)} Rows from {tableName}')
		input()
		table.remove_rows(0)

	# Variables Flags for prints
	oldS = 0
	oldC = 0
	oldSu = 0

	# Variables Timers for prints
	clock = T.Clock(4)
	for i in range(4):
		clock.startTimer(i)

	#start, startS, startC, startSu = t.time_ns(), t.time_ns(), t.time_ns(), t.time_ns()


	for jsonDir in os.listdir(jsonDirDir):
		s,c,su,r,a, _ = parseVideoName(jsonDir)
		setup = s
		videoPath = os.path.join(videosDirDir, f'{PREF_DIR}{setup:03}', MIDDIR, f'{jsonDir}.avi')

		jr = JR.JsonReader(jsonDir, os.path.join(jsonDirDir, jsonDir),videoPath)
		jr.read()

		dataset.addPosRowsArr( jr.data, jsonDir, tableName)

		#Prints
		oldS, oldC, oldSu =	T.Prints(oldS, oldC, oldSu, s,c,su, clock)

	clock.stopTimer(0)
		
def updateRows():
	jsonDirDir = 'C:/Users/up201/Desktop/Tese/openposeOriginal/openpose-master/examples/media/json3/'
	clock = T.Clock(3)
	table2UpName = 'Pos'
	handler = TH.TableHandler()
	file = handler.tables
	if not file.root.__contains__(table2UpName):
		file.root.PosRaw._f_copy(newname = table2UpName)
	with open('Artifacts/rows_2D_update_A4050.txt', 'r') as f:
		lines = f.readlines()
		names = []
		avg = []
		jnosList = []
		for line in lines:
			name,rest = line.split(' ',1)

			#print('name: ', name)
			
			avg_pos, rest =rest.split(') ',1)
			
			avg_pos = avg_pos.replace('(','')
			avg_pos = avg_pos.split(', ')
			avg_pos = (float(avg_pos[0]),float(avg_pos[1]))
			#print('avg_pos: ', avg_pos)

			rest = rest.replace('[','')
			rest = rest.replace(']','')
			rest = [int(n) for n in rest.split(', ')]
			#print('rest: ',rest)

			names += [name]
			avg += [ avg_pos]
			jnosList += [rest]

	names_ex=[]
	if os.path.isfile('Artifacts/rowsUpdated_A4050.txt'):
		with open('Artifacts/rowsUpdated_A4050.txt', 'r') as f:
		 	lines = f.readlines()
		 	names_ex = [line.split(' ')[0] for line in lines]

	clock.startTimer(0)
	clock.startTimer(1)
	clock.startTimer(2)
	print(len(names))
	print(np.array(names))
	print(np.array(names_ex))
	names2run = np.setdiff1d(np.array(names), np.array(names_ex))
#	print(names)
	print(names2run)
	for i, (vname, a, jnos) in enumerate(zip(names, avg, jnosList)):
		path = jsonDirDir + vname + '_rgb/'

		updater = uR.Updater(table_name = table2UpName )
		cond = vname not in names_ex

		if cond:
			updater.updateRows(vname, jnos, a, path, verbose = False)
		else:
			print(vname, names_ex.index(vname))
			pass
		
		if (i+1)%10 == 0:
			clock.startTimer(clock.stopTimer(1,f'Video {i+1} of {len(names)} updated in(10):', verbose = True))

	clock.stopTimer(0,'Total Duration', verbose = True)

if __name__ == '__main__':
	#buildDataset()

	updateRows()