import os
import numpy as np
import sys
import math

sys.path.append('C:/Users/up201/chem/myClasses/')
import tableHandler as TH
import myTime as T

TXTLOCATION = 'Artifacts/Temporary/ReRunOP.txt'
LOADLOCATION= 'Artifacts/Temporary/ReRunOP_ind.txt'
JSONFOLDER = 'C:/Users/up201/Desktop/Tese/openposeOriginal/openpose-master/examples/media/'
PREV = ['json3/', '_rgb/']
ACTUAL = ['json4/', '/']

savedFile = True

folderNames = np.loadtxt(TXTLOCATION, dtype = str)
clock = T.Clock(2)


check = np.zeros(folderNames.shape, dtype= bool)
handler = TH.TableHandler()
a= []
clock.startTimer(0)
condition = '0'
lengs = 0
if not savedFile:
	clock.startTimer(1)
	for i,fn in enumerate(folderNames):
		name = fn[41:-8]
	
		condition = f'vname == \'{name}\''
		#print(condition)

		b = handler.tables.root.Pos.get_where_list(condition)

		with open(LOADLOCATION, "a") as f:
			for ind in b.astype(int):
				f.write(f'{name} {ind}\n')
		a+= [len(b)]

		if (i+1) % 10 == 0:
			clock.startTimer(clock.stopTimer(1,'Batch Duration', verbose = True))
			print(i,'/', folderNames.shape[0])

else:
	jsonIndex = np.loadtxt(LOADLOCATION, dtype = str)
	print(jsonIndex.shape, jsonIndex[0])
	N = 100
	batch_size = len(folderNames)//N
	indexes = np.arange((len(folderNames)))
	leng = np.ceil(len(folderNames) / float(batch_size)).astype(int)
	print('leng:', leng, 'batch_size:', batch_size)
	for idx in range( leng  ):
		#print(idx * batch_size , (idx+1) * batch_size)
		ind = indexes[idx * batch_size : (idx+1) * batch_size]
		for i,ind_i in enumerate(ind):
			name = folderNames[ind_i][41:-8]	
			if  i == 0:
				condition =f'(vname == \'{name}\')'
			else:
				condition += f' | (vname == \'{name}\')'
		print('Get Where')
		b = handler.tables.root.Pos.get_where_list(condition)
		lengs += len(b)
		print(len(b), lengs)

		#print(condition)
	print(f'{lengs} == {len(jsonIndex)} ? {lengs == len(jsonIndex )} ')



clock.stopTimer(0,'Total Duration', verbose =True)

