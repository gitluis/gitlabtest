import os
import numpy as np

TXTLOCATION = 'Artifacts/Temporary/ReRunOP.txt'
JSONFOLDER = 'C:/Users/up201/Desktop/Tese/openposeOriginal/openpose-master/examples/media/'
PREV = ['json3/', '_rgb/']

ACTUAL = ['json4/', '/']
folderNames = np.loadtxt(TXTLOCATION, dtype = str)
check = np.zeros(folderNames.shape, dtype= bool)
#for fn in folderNames:
for i,fn in enumerate(folderNames):
	name = fn[41:-8]
	
	#Get number of files in previous video
	previouspath = JSONFOLDER + PREV[0] + name + PREV[1]
	if os.path.isdir(previouspath):
		n_prev = len(os.listdir(previouspath))
	else:
		print(f'Previous path: {previouspath}')

	#Get number of files in actual video
	actualpath = JSONFOLDER + ACTUAL[0] + name + ACTUAL[1]
	if os.path.isdir(actualpath):
		n_actual = len(os.listdir(actualpath))
	else:
		print(f'Actual path: {actualpath}')
	
	#compare
	check[i]= not ( n_actual  ==  n_prev)

	folderNames[i] = name
	if (i+1) % 100 == 0:
		print(i,'/', folderNames.shape[0])

	if (i>202) & (i< 207):
		print(i,name, n_actual, n_prev)

print(np.where(check))