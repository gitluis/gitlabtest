import numpy as np
import os 
import sys

sys.path.append('C:/Users/up201/chem/myClasses/')
import tableHandler as TH
import myTime as T

TXTLOCATION = 'Artifacts/Temporary/ReRunOP.txt'
LOADLOCATION= 'Artifacts/Temporary/ReRunOP_ind.txt'
JSONFOLDER = 'C:/Users/up201/Desktop/Tese/openposeOriginal/openpose-master/examples/media/'
ACTUAL = ['json4/', '/']


TxtFile = np.loadtxt(LOADLOCATION, dtype = str)
clock = T.Clock(2)

names = TxtFile[:,0]
names_u = np.unique(names)

dirList = os.listdir(JSONFOLDER + ACTUAL[0])
print(len(dirList), dirList[0])

counter = 0
total = len(names_u)
clock.startTimer(0)
clock.startTimer(1)
for name in names_u:
	if name in dirList:
		#print('_'*100)
		dirToRename = JSONFOLDER+ACTUAL[0]+name
		newDirName = dirToRename + '_rgb'
		#print(f'{dirToRename}\nto\n{newDirName}')

		os.rename(dirToRename, newDirName)

		counter +=1
	if counter %100 == 0:
		clock.startTimer(clock.stopTimer(1,f'{counter} / {total}', verbose = True))

clock.stopTimer(0, 'Total', verbose  =True)
print(counter)