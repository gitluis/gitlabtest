import numpy as np

from tensorflow.keras.models import Sequential
from tensorflow.compat.v1.keras.layers import CuDNNLSTM
from tensorflow.keras.layers import Dropout, BatchNormalization, Dense, LSTM
from tensorflow.keras.losses import SparseCategoricalCrossentropy as SCC
from tensorflow.keras.metrics import SparseTopKCategoricalAccuracy as STKCA
from tensorflow.keras.activations import softmax
from tensorflow.nn import relu, sigmoid
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam

from tensorflow.keras.callbacks import ModelCheckpoint, CSVLogger

import sys
sys.path.append('C:/Users/up201/chem/myClasses/')
import tableHandler as TH
import generator as GEN
import myTime as T
import Plotter as PL


def buildModel(nf, ns, nt, mname):

    modl = Sequential()  
    #print(nt)
    # modl.add(mL.tf.compat.v1.keras.layers.CuDNNLSTM(100, input_shape=(ns, nf), return_sequences=True))
    # modl.add(mL.tf.compat.v1.keras.layers.CuDNNLSTM(100, input_shape=(ns, ns)))
    # modl.add(mL.keras.layers.Dropout(0.5))
    # modl.add(mL.keras.layers.Dense(100, activation=mL.tf.nn.relu))
    # modl.add(mL.keras.layers.Dense(nt, activation=mL.tf.keras.activations.softmax)) # mL.tf_nn.sigmoid

 
    #previous : 
    lstm_out = 256
    #modl.add(Dense(nf, input_shape=(ns,nf),activation=softmax )) # mL.tf_nn.sigmoid
    #modl.add(BatchNormalization())
    #modl.add(CuDNNLSTM( lstm_out, input_shape=(ns, nf), return_sequences=True))
    modl.add(LSTM( lstm_out, input_shape=(ns, nf), return_sequences=True))
    #modl.add(Dropout(0.5))
    modl.add(BatchNormalization())

    #modl.add(CuDNNLSTM( lstm_out, input_shape=(ns, nf), return_sequences=True))
    #modl.add(mL.LSTM( 256, input_shape=(ns, nf), return_sequences=True))
    #modl.add(Dropout(0.5))
    #modl.add(BatchNormalization())

    #modl.add(CuDNNLSTM( lstm_out  , input_shape=(ns, nf)        ))    
    modl.add(LSTM( lstm_out  , input_shape=(ns, nf)        ))    
    modl.add(Dropout(0.5))
    modl.add(BatchNormalization())

    modl.add(Dense(lstm_out, activation=relu))
    modl.add(Dense(nt, activation= softmax )) # mL.tf_nn.sigmoid


    #model.add(mL.LSTM((100), input_shape=(2, 50),return_sequences=True))
    #model.add(mL.Dense((50)))


    lfn= SCC()
    m = STKCA(k=1)
    optimizer = Adam(epsilon=0.1)
    modl._name = mname

    modl.compile(loss=lfn, optimizer=optimizer,metrics=[m])
    return modl

def train_model(tr_gen, te_gen , batch_size, model, mname,history_path, n_epochs):
	
	clock = T.Clock(1)

	checkpoint_filepath= '{epoch:02d}-{sparse_top_k_categorical_accuracy:.3f}' # sparse_top_k_categorical_accuracy

	checkpoint = ModelCheckpoint(f'{history_path}Checkpoints/{mname}'+'_{}.h5'.format(checkpoint_filepath, monitor= 'val_sparse_top_k_categorical_accuracy', verbose = 1, save_best_only=True, mode = 'max')) #val_sparse_top_k_categorical_accuracy
	csv_Logger = CSVLogger( f'{history_path}{mname}_history.csv', append = True, separator = ',')

	clock.startTimer(0, 'Start Training', verbose = True)

	h=model.fit(  x= tr_gen, 
		epochs=n_epochs, verbose=1,
		validation_data = te_gen,
		callbacks = [checkpoint, csv_Logger] )

	clock.stopTimer(0, 'Finish Training', verbose = True)

	model.save(mname+'.h5')
	print("Saved model to disk")

	return model


def predictModel(modelpath, inputs, verbose=0):
    model = load_model(modelpath+'.h5')
    pred = model.predict(inputs, verbose = verbose)
    return pred

if __name__ == '__main__':
	nfeat = 30
	nsteps = 11
	ntarg = 50
	batch_size = 1024
	n_epochs = 1
	modelName = 'model_reduced_only_LastDROPBN_2lstm2'
	history_path = 'Models/'

	handler = TH.TableHandler()
	pl = PL.Plotter()
	train_table = handler.tables.root.splits.WTrain1P
	test_table = handler.tables.root.splits.WTest1P
	meta_table = handler.tables.root.Metadata
	window_table = handler.tables.root.PosZeros

	cand = meta_table.get_where_list('action <50')
	myList= np.where(np.isnan(window_table[cand]['pos']))[0]
	print(myList)
	print(len(myList))

	print(window_table[myList[0]])
	input()


	train_generator = GEN.Input(train_table, batch_size)
	test_generator = GEN.Input(test_table, batch_size)

	model = buildModel(nfeat, nsteps, ntarg, modelName)

	model.summary()

	print(model.layers[0].get_weights())
	mname = modelName
	tr_gen = train_generator
	for n in range(165, len(train_generator)):
		checkpoint_filepath= '{epoch:02d}-{sparse_top_k_categorical_accuracy:.3f}' # sparse_top_k_categorical_accuracy

		checkpoint = ModelCheckpoint(f'{history_path}Checkpoints/{mname}'+'_{}.h5'.format(checkpoint_filepath, monitor= 'val_sparse_top_k_categorical_accuracy', verbose = 1, save_best_only=True, mode = 'max')) #val_sparse_top_k_categorical_accuracy
		csv_Logger = CSVLogger( f'{history_path}{mname}_history.csv', append = True, separator = ',')

		h=model.train_on_batch(  x= tr_gen[n][0], y = tr_gen[n][1], reset_metrics = False )

	#model = train_model(train_generator, test_generator, batch_size, model, modelName, history_path,n_epochs)
		print(model.layers[0].get_weights())
		print('---'*50 + f'{n}')
	#pl.plotAccLoss(history_path, modelName)




	#predict = predictModel(history_path+modelname, test_generator, verbose = 1)
	#i = np.argmax(predict ,axis = 1)
	#printConfusionMatrix(target[val_set],i)

