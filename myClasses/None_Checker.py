import numpy as np
import sys
from matplotlib import pyplot as plt
import math

import Plotter as Pl
import tableHandler as tH 
import myTime as T

def AddCondition(old, new=None, operator = '&'):
	if new:
		return f'{old} {operator} ({new})'
	return old



class None_Checker:
	def __init__(self,condition = 'pco == 0'):
		th = tH.TableHandler()
		self.meta_table = th.tables.root.Metadata
		self.pos_table  = th.tables.root.PosZeros
		self.condition = f'({condition})'
		print(self.condition)
		self.None_location = self.meta_table.get_where_list(self.condition)
		
	def countCol(self, colname, preConditons=[], returnAll =False):
		counter  = Counter(self.condition)
		counter.updateBaseCondition(preConditons)
		#print(counter.condition)
		None_location = self.meta_table.get_where_list(counter.condition)
		return counter.countColumn(self.meta_table, self.None_location if returnAll else None_location, colname)
	
	def getNoneVideoLengs(self, verbose = False):


		
		meta_table = self.meta_table
		
		old_s, old_c, old_su=0,0,0
		clock = T.Clock(4)

		
		for n in range(4):
			clock.startTimer(n)


		a,b,c,d,e = meta_table.col('setup')[self.None_location].reshape((-1,1)), meta_table.col('camera')[self.None_location].reshape((-1,1)), meta_table.col('subject')[self.None_location].reshape((-1,1)), meta_table.col('replication')[self.None_location].reshape((-1,1)), meta_table.col('action')[self.None_location].reshape((-1,1))

		cond_data = np.hstack((a,b,c,d,e))
		cond_data = np.unique(cond_data, axis = 0)
		
		data = np.zeros((cond_data.shape[0],cond_data.shape[1]+1), dtype = int)

		for n in range(cond_data.shape[0]):
			newcondition =  f'(setup == {cond_data[n,0]}) & (camera == {cond_data[n,1]}) & (subject == {cond_data[n,2]}) & (replication == {cond_data[n,3]}) & (action == {cond_data[n,4]})'
			leng = len(meta_table.get_where_list(newcondition))
			if leng>0:
				data[n] = cond_data[n,:].tolist() + [leng]

			s,c,su = cond_data[n,0], cond_data[n,1], cond_data[n,2]
			if verbose:
				old_s, old_c, old_su = T.Prints(old_s, old_c, old_su, s,c,su, clock, verbose=verbose)

		clock.stopTimer(0, 'Total', verbose = verbose)

		return data	




class Counter:
	def __init__(self, condition):
		self.condition = condition

	def count(self,meta_table,addConditions=[]):
		newCondition = self.condition
		for cond in addConditions:
			newCondition = AddCondition(newCondition, cond)

		return len(meta_table.get_where_list(newCondition)	)

	def countColumn(self,meta_table, None_location, colname):
		x_bins = np.unique(meta_table.col(colname)[None_location])
		data = np.zeros((x_bins.shape[0], 2))

		#print(colname, x_bins.shape, data.shape)
		data[:,0] = x_bins
		for i,setup in enumerate(x_bins):
			data[i,1]=self.count(meta_table,[f'{colname} == {setup}'])

		return data

	def updateBaseCondition(self,conditions=[]):
		for n in conditions:
			self.condition = AddCondition(self.condition, n)







if __name__ == '__main__':
	pass
