import tables

class Sample(tables.IsDescription):
	setup = tables.IntCol(pos = 0)
	camera = tables.IntCol(pos = 1)
	subject = tables.IntCol(pos = 2)
	replication = tables.IntCol(pos = 3)
	action = tables.IntCol(pos = 4)

	jno = tables.IntCol(pos = 5)
	pco = tables.IntCol(pos = 6)	
	
