import tables

class Sample(tables.IsDescription):
	x_labels =  tables.StringCol(20, pos = 0)
	y_labels =  tables.StringCol(20, pos = 1)
	
	x_values = tables.FloatCol(pos = 2)
	y_values = tables.FloatCol(pos = 3)
