import tables

all_subjects = list(range(1,41))
train_subjects = [ 1, 2, 4, 5, 8, 9, 13, 14, 15, 16, 17, 18, 19, 25, 27, 28, 31, 34, 35, 38]
test_subjects = [n for n in all_subjects if n not in train_subjects]

all_views = list(range(1,4))
train_views = [2,3]
test_views = [n for n in all_views if n not in train_views]

class Sample(tables.IsDescription):
	data = tables.FloatCol(shape=(30,), pos = 0)
	label   =  tables.IntCol( pos = 1)


class W_Sample(tables.IsDescription):
	data = tables.FloatCol(shape=(11,30), pos = 0)
	label   =  tables.IntCol( pos = 1)