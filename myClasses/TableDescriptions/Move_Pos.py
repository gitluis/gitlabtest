import tables

class Sample(tables.IsDescription):
	setup = tables.IntCol(pos = 0)
	camera = tables.IntCol(pos = 1)
	subject = tables.IntCol(pos = 2)
	replication = tables.IntCol(pos = 3)
	action = tables.IntCol(pos = 4)
	vName =  tables.StringCol(20, pos = 5)	

	move_max = tables.FloatCol(pos = 6)

