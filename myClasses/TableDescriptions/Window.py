import tables

class Sample(tables.IsDescription):
	pos = tables.FloatCol(shape=(11,30), pos = 0)

	setup = tables.IntCol(pos = 1)
	camera = tables.IntCol(pos = 2)
	subject = tables.IntCol(pos = 3)
	replication = tables.IntCol(pos = 4)
	action = tables.IntCol(pos = 5)
	
	jno = tables.IntCol(pos = 6)