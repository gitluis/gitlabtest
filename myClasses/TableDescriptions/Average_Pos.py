import tables

class Sample(tables.IsDescription):
	vName =  tables.StringCol(20, pos = 0)
	jno   =  tables.IntCol( pos = 1)
	
	x_average = tables.FloatCol(pos = 2)
	y_average = tables.FloatCol(pos = 3)
