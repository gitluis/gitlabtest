import os
import numpy as np
from moviepy.editor import VideoFileClip as VLC
import sys
sys.path.append('/Users/up201/chem')
import myLib as mL

MIDDIR = 'nturgb+d_rgb'
JSONSDIR = 'C:/Users/up201/Desktop/Tese/openposeOriginal/openpose-master/examples/media/json3'

def getMinMaxRatio(ratios): 
	# Input: ratios np array
	# Output: Tuple(Min, Max)
	# Ignore ratios == 0, ratios are calculated by number of jsons / video duration
	
	indexes = np.where(ratios>0);
	return np.min(ratios[indexes], ratio[indexes])
	
def printHeatMap(hm_pred, savepath = None):
	# Input: np array
	# Output: Show heatmap of hm_pred with grid
    
    ax= mL.sns.heatmap(hm_pred, cmap= mL.plt.get_cmap('RdYlGn'))
    mL.plt.grid()
    ax2 = mL.plt.gca()

    # Align grid xticks
    ax2.set_yticks( ax2.get_yticks()-0.5) 
    ax2.set_xticks(  ax2.get_xticks()-0.5) 

    mL.plt.show()
    if savepath != None:
    	fig = mL.plt.gcf()
    	fig.save(savepath)

def parseVideoName(name, verbose = 0):
	# Input : Video_name  Verbose
	# Output: Tupe(Setup, Camera, Subject, Replication, Action, Rest of the name as String )
	s = int(name[1:4])
	c= int(name[5:8])
	su= int(name[9:12])
	r= int(name[13:16])
	a= int(name[17:20])
	rest = name[20:]

	if verbose:
		print(f'Name: {name}\tSet: {s}, Cam: {c}, Sub: {su}, Rep: {r}, Act: {a}, Rest: {rest} ')
		input()
	return s,c,su,r,a, rest

class NumberOfVideos:
	def __init__(self, path = 'D:/dataset'):

		self.path = path
		self.setups = 17
		self.cameras = 3
		self.subjects = 40
		self.replications = 2
		self.actions = 60

		self.metaData = np.zeros((self.setups, self.cameras, self.subjects, self.replications, self.actions), dtype = bool)

		if(os.path.isdir(path)):
			for setup in os.listdir(path): # Run each Setup folder
				
				setVideos_path = os.path.join(path, setup)
				setVideos_path = os.path.join(setVideos_path, MIDDIR ) #every setup folder has one folder with the videos
				
				if os.path.isdir(setVideos_path):
					for video in os.listdir(setVideos_path): #For every video in setup

						s,c,su,r,a,_ = parseVideoName(video)
						self.metaData[s-1,c-1,su-1,r-1,a-1] = True # set true if that action axists

				else:
					raise Exception(f'{setVideos_path} doesn\'t exist')
		else:
			raise Exception(f'{path} doesn\'t exist')

		self.n_videos = np.sum(self.metaData)  # number of Trues in metadata = number of videos
		
		self.SS_Matrix = np.zeros((self.setups, self.subjects))  # Setup-Subject Matrix, Element =True if Setup has Subject
		aux  = np.argwhere(np.sum(self.metaData, axis = (1,3,4)) == self.cameras * self.actions * self.replications)
		for coordinate in aux:
			self.SS_Matrix[coordinate[0], coordinate[1]] = 1

	def __repr__(self,): # in Prints, prints the metadata
		return self.metaData.__repr__()

	def getSliceCount(self,St = [None,None], C = [None,None], Su =[None,None], R=[None,None], A=[None,None]): # Count number of videos in a slice
		a= self.metaData[ St[0]:St[1], C[0]:C[1], Su[0]:Su[1], R[0]:R[1], A[0]:A[1] ]
		return np.sum(a)

	def _getVideoJSRatio(self, video_path, jsons_path, verbose = 0): 
		#		Get video_jsonRatio = jsons /video_duration
		# Input: video_path   jsons_directory_path   Verbose
		# Output Number of jsons / Video duration
		if verbose:
			print(video_path ,'\t|\t', jsons_path)
			

		clip = VLC(video_path) 				# video handler
		jsons = len(os.listdir(jsons_path))	# number of jsons
		
		if verbose:
			print(jsons,'/',clip.duration, '=' , jsons/clip.duration)
			input()

		return jsons/clip.duration

	def getVideosJSRatio(self,):
		#Initiate ratios np array
		ratios = np.zeros(self.metaData.shape, dtype = float)

		# Variables Flags for prints
		oldS = 0
		oldC = 0
		oldSu = 0

		# Variables Timers for prints
		start =mL.t.time_ns()
		startS = mL.t.time_ns()
		startC = mL.t.time_ns()
		startSu = mL.t.time_ns()

		
		for setup in os.listdir(self.path):

			setVideos_path = os.path.join(self.path, setup)
			setVideos_path = os.path.join(setVideos_path, MIDDIR )
			
			for video in os.listdir(setVideos_path):
				s,c,su,r,a ,_ = parseVideoName(video)

				ratios [s-1,c-1,su-1,r-1,a-1] = self._getVideoJSRatio(os.path.join(setVideos_path,video),os.path.join(JSONSDIR, video[:-4]) )
				
				# Prints
				if su-1 != oldSu:
					durSu =mL.t.time_ns() - startSu
					print(f'\t\t\t\tSubject {oldSu+1}', mL.buildTime(mL.ns2ms(durSu)), 'ms')

					oldSu = su-1
					startSu =mL.t.time_ns()
			
				if c-1 != oldC:
					durC =mL.t.time_ns() - startC
					print(f'\t\tCamera {oldC+1}: ', mL.buildTime(mL.ns2ms(durC)), 'ms')

					oldC = c-1
					startC =mL.t.time_ns()

				if s-1 != oldS:
					durS =mL.t.time_ns() - startS
					print(f'Setup {oldS+1}: ', mL.buildTime(mL.ns2ms(durS)), 'ms')
					
					oldS = s-1
					startS =mL.t.time_ns()


		dur = mL.t.time_ns() - start
		print(mL.buildTime(mL.ns2ms(dur)), 'ms')
		
		np.save('Artifacts/Ratios.npy', ratios)
		
		return ratios

	def getSubjectsInSetup(self,setup):
		# Input: setup Number
		# Output setup subjects indexes
		return np.argwhere(self.SS_Matrix[setup,:]).flatten()

	def getSubjectsList(self,):
		#Outputs: List of length setups where each element is the indexes of existing subjects in setup
		ret = []

		for n in range(self.setups):
			ret += [self.getSubjectsInSetup(n)]
		return ret

