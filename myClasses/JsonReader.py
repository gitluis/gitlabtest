import numpy as np
import os
import json
import numpy.linalg as linalg


def extractJson(filename, verbose = 0, avg = None):
	# Input: file path  verbose type
	# Output: Tuple (X, Y, jno, pco)

	# Specifics: If no persons are detected, X and Y are none, pco = 0, and jno = json number

	weights = np.ones((15,))
	weights[1], weights[2], weights[5], weights[8], weights[9], weights[12] = 3,2,2,3,2,2
	
	with open(filename) as f:													# Load Json
		data = json.load(f)														

	pco = len(data['people'])													# get number of people
	jno = int(filename[-15-12:-15])												# get json number

	if verbose:
		print(f'File path: {filename}\n PCO = {pco}\t JNO = {jno}', pco)
	
	if pco >0:			
		if avg:	
			dists = []			
			XX=[]
			YY= []								
			for p in range(pco):
				pose_data = data['people'][p]['pose_keypoints_2d']						# get Position Coordinates (x,y,confidense)

				xCoordinates = [n for i,n in enumerate(pose_data) if (i%3 == 0) and (i//3 <15)]		# get X Coordinates
				yCoordinates = [n for i,n in enumerate(pose_data) if ((i+2)%3 == 0) and (i//3 <15)]	# get Y Coordinates
				XX += [xCoordinates]
				YY += [yCoordinates]
				X=np.array(xCoordinates)
				Y = np.array(yCoordinates)
				a_x = np.average(X, weights = weights)
				a_y = -1*np.average(Y, weights = weights)+1080
				
				norma = linalg.norm(np.array(avg)-np.array([a_x,a_y]))
				
				dists+=[norma]

			p = np.array(dists).argmin()
			#print(p)
			xCoordinates = XX[p]
			yCoordinates = YY[p]

			#input()
		else:
			p=0
			pose_data = data['people'][p]['pose_keypoints_2d']						# get Position Coordinates (x,y,confidense)

			xCoordinates = [n for i,n in enumerate(pose_data) if (i%3 == 0) and (i//3 <15)]		# get X Coordinates
			yCoordinates = [n for i,n in enumerate(pose_data) if ((i+2)%3 == 0) and (i//3 <15)]	# get Y Coordinates
		
		return (xCoordinates, yCoordinates, jno, pco)	
	else:
		return (None, None, jno, pco)



class JsonReader:
	def __init__ (self,Action='', JsonsDirectory = None, Video_path = None):
		# Input: Action, in case of test videos its a default path to jsons and videos
		#		 JsonDirectory
		#        Video_path
		
		if (JsonsDirectory==None) & (Video_path==None):
			JsonsDirectory = f'Resources/OpenPose_Test_Videos/output/{Action}'
			Video_path = f'Resources/OpenPose_Test_Videos/{Action}.avi'

		if not os.path.isdir(JsonsDirectory):
			raise Exception(f'{JsonsDirectory} does not exist')
	
		self.jsons_directory = JsonsDirectory
		self.video_path = Video_path
		self.name = Action
		self.data = np.zeros((len(os.listdir(JsonsDirectory)), 30+2)) # Coordinates(30) + jno + pco

	def read(self, window = None, avg = None):
		if not window:
			window = list(range(len(os.listdir(self.jsons_directory))))

		w_i = 0
		for i, file in enumerate(os.listdir(self.jsons_directory)): # enumerate jsons file in jsons directory
			if i in window:
				Coord =[None]*30										# initiate Coord as a 30 length None vector
			
				x,y, jno, pco = extractJson(os.path.join(self.jsons_directory,file), avg = avg) # Extract data from json
				w_i +=1
			
				if (x != None) & (y!=None):	# if not empty change None Values to extracted values
					Coord[0::2] = x
					Coord[1::2] = y

				#			 			Build Data	
				self.data[i,:30]= Coord 	 
				self.data[i,30:32] = np.array([jno, pco])

