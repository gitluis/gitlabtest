import matplotlib.pyplot as plt
import Plotter as Pl
import numpy as np
import seaborn as sns
from matplotlib.colors import LinearSegmentedColormap as LSC
import colorsys

N = 60
HSV_tuples = [(x*1.0/N, 0.5, 0.5) for x in range(N)]
RGB_tuples = map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples)

CONSTANT= {	'action' :		60,
			'replication':	2,
			'subject':		40,
			'camera':		3,
			'setup':		17
			}

colors = []
for n in CONSTANT:
	N = CONSTANT[n]
	HSV_tuples = [(x*1.0/N, 0.7, 0.8) for x in range(N)]
	RGB_tuples = map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples)
	colors += [list(RGB_tuples)]

COLORS = {
			'action' :		colors[0],
			'replication':	colors[1],
			'subject':		colors[2],
			'camera':		colors[3],
			'setup':		colors[4]

	
		}





class Bar:
	def __init__(self, title = None, savepath=None):
		self.title = title
		self.savepath = savepath

	def plotBar(self,data,  show = True, xlabel = None, ylabel=None, Bottom  = None, label = None, color =None):
		#print(data)
		plt.bar(data[:,0], data[:,1], bottom = Bottom, label = label, color = color)
		#print(color)
		#print('label:',label)

		Pl.configureAxes(xlabel, ylabel)

		plt.title(self.title)
		if self.savepath:
			fig = plt.gcf()
			fig.savefig(self.savepath)
		if show:
			plt.show()

class Bars(Bar):
	def __init__(self,title=None, savepath=None):
		super().__init__(title, savepath)

	def plot(self, data, **kwargs):
		
		self.plotBar(data, **kwargs)

	def parseSP_args(subp, checker, windowed, distrib, preConditions = [], XX='action', show = True, n=0):
		#label =  f'{byColname} {distrib[n,0].astype(int)}'
		kwargs = {'show' : show,  'color': COLORS[windowed][n]}
		#print('Bars')
		return ((distrib,) ,{**kwargs})


class StackedBars(Bar):
	def __init__(self,title=None, savepath=None):
		super().__init__(title, savepath)

	def plot(self, checker, colored, colored_distrib, XX='action', preConditions = [], show = True, verbose = 0):
		bars = []
		labels = []

		leng=CONSTANT[XX]
		bottom = np.zeros((leng,)).astype(int)

		for n in range(colored_distrib.shape[0]):
			forCondition = preConditions + [f'{colored} == {colored_distrib[n,0].astype(int)}']
			bar = checker.countCol(XX, forCondition, returnAll = False )
			#print(bar)
			if verbose >1:
				print(f'\t\tProccess Color {n}:\tNumber of Colors: {colored_distrib.shape[0]}\tNumber of X-Bins: {bar.shape[0]}\t\tCondition: {forCondition}')
			label = f'{colored} {colored_distrib[n,0].astype(int)}'
			if len(bar):
				self.plotBar(bar, show = False, Bottom = bottom[bar[:,0].astype(int)-1] , label = label, color = COLORS[colored][colored_distrib[n,0].astype(int)-1])
			handles_, labels_ = plt.gca().get_legend_handles_labels()
			#print(handles_, labels_)
			
			bottom[bar[:,0].astype(int)-1] += bar[:,1].astype(int)
			
		ylim= np.max(bottom)*1.1

		xlim = leng #np.max(np.where(bottom)[0])*1.1
		#print(bottom)
		#print(np.where(bottom)[0], np.where(bottom))
		#print(xlim)
		#input()

		plt.gca().set_ylim(0,ylim)
		plt.gca().set_xlim(0,xlim)
		
		if len(preConditions):
			plt.title(self.title)

		if show:
			print('show')
			plt.show()
		return ylim


	def parseSP_args(self, checker, colored, colored_distrib, **kwargs):
		new_kwargs = {}
		#print('StackedBars')

		if 'XX' in kwargs:
			new_kwargs = {**new_kwargs, 'XX': kwargs.get('XX')}
		if 'show' in kwargs:
			new_kwargs = {**new_kwargs, 'show': kwargs.get('show')}
		if 'preConditions' in kwargs:
			new_kwargs = {**new_kwargs, 'preConditions': kwargs.get('preConditions')}

		return ((checker, colored, colored_distrib), new_kwargs)


class HeatMap(Bar):
	def __init__(self,title = None, savepath= None):
		super().__init__(title, savepath)
		self.shape = (0,)

	def plot(self,hm_pred, names=None, show = True):
		self.shape = hm_pred.shape
		colors = [(0,0,0), (0.8,0.8,0.8), (1,0,0)]
		cm = LSC.from_list('BWR', colors,  N= len(colors))
		ax= sns.heatmap(hm_pred, yticklabels= names ,cmap= cm) #plt.get_cmap('RdYlGn')
		ax.set(xlabel = 'Jsons')
		if show:
			plt.show()
		return ax

	def parseSP_args(self,names, jsons, jsons_len, show = False):
		json_max = jsons_len.max() +1
		hm = np.zeros((len(names), json_max))

		for row,leng in enumerate(jsons_len):
			hm[row, leng:] = -1
		for row,video_jsons in enumerate(jsons):
			for jno in video_jsons:
				hm[row,jno] = 1
		
		return ((hm, names), {'show':show})
	