import tables as tb
import numpy as np
import os 
import time as t
import math
import matplotlib.pyplot as plt
import sys
sys.path.append('myClasses/TableDescriptions')
import Pos as  POS
import Lengs as LENGS
import Histogram as HISTOGRAM
import Average_Pos as AP
import Move_Pos as MP
import Metadata as META
import Splits as SPLITS
import Window as WIND
import myTime as T

def parseVideoName(name, verbose = 0):
	# Input : Video_name  Verbose
	# Output: Tupe(Setup, Camera, Subject, Replication, Action, Rest of the name as String )
	s = int(name[1:4])
	c= int(name[5:8])
	su= int(name[9:12])
	r= int(name[13:16])
	a= int(name[17:20])
	rest = name[20:]

	if verbose:
		print(f'Name: {name}\tSet: {s}, Cam: {c}, Sub: {su}, Rep: {r}, Act: {a}, Rest: {rest} ')
		input()
	return s,c,su,r,a, rest





class TableHandler():
	def __init__(self, path = '/Tables/dataset.h5', description = POS.Sample):
		self.path = path
		if not os.path.isfile(self.path):
			self.tables = tb.open_file(self.path, 'a')
			self.tables.create_table(self.tables.root, 'Pos', description=description)
			self.tables.create_group('/', 'hist', 'Histograms')
			self.tables.create_group('/hist/', 'zeros', 'Zero Distribution')
			self.tables.create_group('/', '/average', 'Average Position Per Json')
			self.tables.flush()
		else:
			self.tables =tb.open_file(self.path, 'a')

	def addPosRowsArr(self, data, name, tableName = 'Pos'):
		table =self.tables.root._f_get_child(tableName)
		rowList = [None]* data.shape[0]
		
		for n in range(data.shape[0]):
			row = (data[n,:30], data[n,30], data[n,31], name )	
			rowList[n] = row
		
		table.append(rowList)
		table.flush()

	def buildMetadata(self,name = 'Metadata', source = 'Pos'):
		
		if not self.tables.__contains__(f'/{name}'):
			self.tables.create_table(self.tables.root, name, description=META.Sample)
		
		table_source = self.tables.root._f_get_child(f'/{source}')
		table_meta = self.tables.root._f_get_child(f'/{name}')
		
		# Variables Flags for prints
		oldS, oldC, oldSu = 0, 0, 0
		
		# Variables Timers for prints
		clock = T.Clock(4)
		for i in range(4):
			clock.startTimer(i)

		#start, startS, startC, startSu = t.time_ns(), t.time_ns(), t.time_ns(), t.time_ns()

		for i, row in enumerate(table_source.iterrows()):
			s,c,su,r,a, _=parseVideoName(row['vname'])
			
			newrow =(s,c,su,r,a,row['jno'], row['pco'])
			table_meta.append([newrow])
			table_meta.flush()
			oldS, oldC, oldSu =	T.Prints(oldS, oldC, oldSu, s,c,su, clock)

		clock.stopTimer(0,'Total')

	def buildLengs(self, name = 'VideoLengths', source = ['Pos', 'Metadata'], sample = LENGS.Sample):
		if not self.tables.__contains__(f'/{name}'):
			self.tables.create_table(self.tables.root, name, description=sample)
		
		table_sources = {}
		for s in source:
			table_sources = {**{f'{s}' : self.tables.root._f_get_child(f'/{s}')}, **table_sources}

		table_lengs = self.tables.root._f_get_child(f'/{name}')

		# Variables Flags for prints
		oldS, oldC, oldSu, oldR, oldA = 0, 0, 0, 0, 0
		oldVideo = {'oldS': 1,
					'oldC': 1,
					'oldSu': 1,
					'oldR': 1,
					'oldA': 1}
		
		# Variables Timers for prints
		clock = T.Clock(4)
		for i in range(4):
			clock.startTimer(i)
		print(table_sources)
		for i, (Prow, Mrow) in enumerate( zip(table_sources.get(source[0]).iterrows(), table_sources.get(source[1]).iterrows() )  ):
			
			s,c,su,r,a = Mrow['setup'],Mrow['camera'],Mrow['subject'],Mrow['replication'],Mrow['action']
			curVideo =  {	'oldS': s,
							'oldC': c,
							'oldSu': su,
							'oldR': r,
							'oldA': a 	}
			

			if curVideo != oldVideo:
				leng = i - beg

				newrow =( *tosave_meta, leng, beg, tosave_name)
				table_lengs.append([newrow])
				table_lengs.flush()
				
				oldS, oldC, oldSu =	T.Prints(oldS, oldC, oldSu, s,c,su, clock)
				
				oldVideo = {'oldS': s,
					'oldC': c,
					'oldSu': su,
					'oldR': r,
					'oldA': a}
			if Mrow['jno'] ==0 :
				beg = i
				tosave_meta = Mrow['setup'],Mrow['camera'],Mrow['subject'],Mrow['replication'],Mrow['action']
				tosave_name = Prow['vname']
		leng = len(table_sources.get(source[0])) - beg
		newrow =( *tosave_meta, leng, beg, tosave_name)
		table_lengs.append([newrow])
		table_lengs.flush()


		clock.stopTimer(0,'Total')

	def create_Histogram(self,group = 'zeros', name = 'Default', sample = HISTOGRAM.Sample, verbose = True):
		if not self.tables.root.hist.__contains__(f'{group}'):
			self.tables.create_group('/', group, group+'_name')

		location = self.tables.get_node(f'/hist/{group}')
		
		if not location.__contains__(f'{name}'):
			self.tables.create_table(location, name, description=sample)
		return self.tables.get_node(f'/hist/{group}/{name}')


	def buildHistogram(self,histTable, data, xlabels = '', ylabels = '', verbose = True):
		clock = T.Clock(1)
		clock.startTimer(0)
		X = data[:,0]
		Y = data[:,1]
		for x,y in zip(X, Y):		
			row = (xlabels, ylabels, x,y)
			histTable.append([row])
			histTable.flush()
		clock.stopTimer(0,'Histogram built',verbose = verbose)


	def create_Average(self, name = 'pos', sample = AP.Sample, verbose = True):
		location = self.tables.root.average
		if not location.__contains__(f'{name}'):
			self.tables.create_table(location, name, description=sample)
		self.tables.flush()
		return self.tables.get_node(f'/average/{name}')


	def build_Average(self,):

		pos_table = self.tables.root.PosRaw
		average_table = self.tables.root.average.pos
		clock= T.Clock(2)


		weights = np.ones((15,))
		weights[1], weights[2], weights[5], weights[8], weights[9], weights[12] = 3,2,2,3,2,2
		i=0
		leng = len(pos_table)

		clock.startTimer(0)
		clock.startTimer(1)
		for row in pos_table.iterrows():
			a = row['pos']
			x_avg = np.average(a[::2], axis = 0, weights = weights)
			y_avg = -1*np.average(a[1::2], axis = 0, weights = weights)+1080
			
			new_row = (row['vname'].decode('utf-8'), row['jno'], x_avg, y_avg)
			
			#print(new_row)
			#print(average_table)

			average_table.append([new_row])	

			average_table.flush()
			
			i+=1
			if (i)% 100000 == 0:
				clock.startTimer(clock.stopTimer(1, f'{i+1} / {leng}', verbose = True))

		clock.stopTimer(0,'Full Duration:', verbose  =True)



	def create_Move(self, name = 'avgMove', sample = MP.Sample, verbose = True):
		location = self.tables.root
		if not location.__contains__(f'{name}'):
			self.tables.create_table(location, name, description=sample)
			self.tables.flush()
		return self.tables.get_node(f'/{name}')

	def build_Move(self,):
		lengs_table = self.tables.root.VideoLengths
		average_table = self.tables.root.average.pos
		move_table = self.tables.root.avgMove
		clock= T.Clock(2)
		i=0

		clock.startTimer(0)
		clock.startTimer(1)
		for row in lengs_table.iterrows():
			ini = row['begining']
			leng = row['length']
			
			video_avg = (average_table.cols.x_average[ini:ini+leng], average_table.cols.y_average[ini:ini+leng])
			xmov, ymov = ( np.absolute( np.diff(video_avg[0])) , np.absolute( np.diff(video_avg[0])) )
			
			xmov = np.square(xmov)
			ymov = np.square(ymov)

			movement = np.sqrt(xmov * ymov)



			if not np.isnan(movement.max()) :
				name = row['vname']
				s,c,su,r,a,_ = parseVideoName(name)

				new_row = (s,c,su,r,a, name.decode('utf-8'), movement.max() )
				move_table.append([new_row])
				move_table.flush()
			i+=1
			if (i)% 10000 == 0:
				clock.startTimer(clock.stopTimer(1, f'{i+1} / {len(lengs_table)}', verbose = True))
		clock.stopTimer(0,'Full Duration:', verbose  =True)

	def vizualize_Move(self,):
		table = self.tables.root.avgMove
		points = table.cols.move_max[:]

		plt.scatter(range(len(points)), points)
		plt.show()
	
	def vizualize_Average(self, vname):
		table = self.tables.root.average.pos 
		table_i = table.get_where_list(f'vname ==\'{vname}\' ')
		points = np.zeros((len(table_i),2))

		for i,row in enumerate(table[table_i]):
			points[i,:] = [row['x_average'], row['y_average']]


		plt.scatter(points[:,0], pointd[:,1])
		plt.show()

	def createSplit(self,name = 'Train',description = SPLITS.Sample):
		self.tables.create_table('/splits', name, description = description)

	def buildTrainSplit(self, ):
		pos_table = self.tables.root.PosZeros
		meta_table = self.tables.root.Metadata
		train_table = self.tables.root.splits.Train1P
		clock= T.Clock(2)
		train_list = []
		for n in SPLITS.train_subjects:
			train_list.extend(meta_table.get_where_list(f'(subject == {n}) & (action<50)'))

		print(len(train_list))
		clock.startTimer(0)
		clock.startTimer(1)
		new_rows = []
		for i,n in enumerate(train_list):
			pos = pos_table.cols.pos[n]
			label = meta_table.cols.action[n]

			new_rows += [(pos,label)]
			
			if (i+1)% 100000==0:
				train_table.append(new_rows)
				new_rows = []
				clock.startTimer(clock.stopTimer(1, f'Written {i+1} of {len(train_list)}\tBatch Duration:', verbose = True))

		train_table.append(new_rows)
		train_table.flush()

		clock.stopTimer(0, 'Total:', verbose = True)

		return train_list

	def buildTestSplit(self, ):
		pos_table = self.tables.root.PosZeros
		meta_table = self.tables.root.Metadata
		test_table = self.tables.root.splits.Test1P
		clock= T.Clock(2)
		
		test_list = []
		for n in SPLITS.test_subjects:
			test_list.extend(meta_table.get_where_list(f'(subject == {n}) & (action<50)'))

		print(len(test_list))
		clock.startTimer(0)
		clock.startTimer(1)
		new_rows = []
		for i,n in enumerate(test_list):
			pos = pos_table.cols.pos[n]
			label = meta_table.cols.action[n]

			new_rows += [(pos,label)]
			

			
			if (i+1)% 100000==0:
				test_table.append(new_rows)
				new_rows = []
				clock.startTimer(clock.stopTimer(1, f'Written {i+1} of {len(test_list)}\tBatch Duration:', verbose = True))

		test_table.append(new_rows)
		test_table.flush()

		clock.stopTimer(0, 'Total:', verbose = True)


		return test_list

	def buildWindowTrainSplit(self, ):
		pos_table = self.tables.root.PosWindow
		train_table = self.tables.root.splits.WTrain1P
		
		clock= T.Clock(2)
		train_list = []
		clock.startTimer(0)
		for n in SPLITS.train_subjects:
			train_list.extend(pos_table.get_where_list(f'(subject == {n}) & (action<50)'))
		clock.stopTimer(0, f'Built {len(train_list)} indices list\tDuration:', verbose = True)
		print(len(train_list))
		clock.startTimer(0)
		clock.startTimer(1)
		new_rows = []
		for i,n in enumerate(train_list):
			prow =pos_table[n] 
	
			pos = prow['pos']
			label = prow['action']

			new_rows += [(pos,label)]
			
			if (i+1)% 100000==0:
				train_table.append(new_rows)
				new_rows = []
				clock.startTimer(clock.stopTimer(1, f'Written {i+1} of {len(train_list)}\tBatch Duration:', verbose = True))

		train_table.append(new_rows)
		train_table.flush()

		clock.stopTimer(0, 'Total:', verbose = True)

		return train_list

	def buildWindowTestSplit(self, ):
		pos_table = self.tables.root.PosWindow
		test_table = self.tables.root.splits.WTest1P
		clock= T.Clock(2)
		
		test_list = []
		clock.startTimer(0)
		for n in SPLITS.test_subjects:
			test_list.extend(pos_table.get_where_list(f'(subject == {n}) & (action<50)'))
		clock.stopTimer(0, f'Built {len(test_list)} indices list\tDuration:', verbose = True)
		print(len(test_list))
		clock.startTimer(0)
		clock.startTimer(1)
		new_rows = []
		for i,n in enumerate(test_list):
			prow =pos_table[n] 
			pos = prow['pos']
			label = prow['action']

			new_rows += [(pos,label)]
			

			
			if (i+1)% 100000==0:
				test_table.append(new_rows)
				new_rows = []
				clock.startTimer(clock.stopTimer(1, f'Written {i+1} of {len(test_list)}\tBatch Duration:', verbose = True))

		test_table.append(new_rows)
		test_table.flush()

		clock.stopTimer(0, 'Total:', verbose = True)


		return test_list

	def createWindow(self,name = 'PosWindow' ,description = WIND.Sample):
		self.tables.create_table('/', name, description = description)

	def buildWindow(self, ):
		pos_table = self.tables.root.PosZeros
		meta_table = self.tables.root.Metadata
		window_table = self.tables.root.PosWindow
		clock= T.Clock(2)

		window_cand = meta_table.get_where_list('(jno >= 10) & (action < 50)')
		
		clock.startTimer(0)
		clock.startTimer(1)
		new_rows = []
		for i,wc in enumerate(window_cand):
			pos = pos_table.cols.pos[wc-10:wc+1]

			mrow = meta_table[wc]

			s = mrow['setup']
			c = mrow['camera']
			su = mrow['subject']
			r = mrow['replication']
			a = mrow['action']
			j = mrow['jno']

			new_rows += [( pos, s, c, su, r, a, j )]
			if (i+1)% 100000==0:
				window_table.append(new_rows)
				new_rows = []
				clock.startTimer(clock.stopTimer(1, f'Written {i+1} of {len(window_cand)}\tBatch Duration:', verbose = True))

		window_table.append(new_rows)
		window_table.flush()

		clock.stopTimer(0, 'Total:', verbose = True)

		return window_cand


if __name__ == '__main__':
	handler = TableHandler()
