import numpy as np
from tensorflow.keras.utils import Sequence
import sys
sys.path.append('C:/Users/up201/chem/myClasses/')
import tableHandler as TH
sys.path.append('C:/Users/up201/chem/myClasses/TableDescriptions')
import Splits as SPLITS



class Input(Sequence) :
  
  def __init__(self, input_table, batch_size) :
    self.input = input_table
    self.batch_size = batch_size
    
    
  def __len__(self) :
    return (np.ceil(len(self.input) / float(self.batch_size))).astype(np.int)
  
  
  def __getitem__(self, idx) :
    batch_x = self.input[ idx * self.batch_size : (idx+1) * self.batch_size ]['data']
    batch_y = self.input[ idx * self.batch_size : (idx+1) * self.batch_size ]['label']-1
    
    #print('\n', batch_y[:1000:100])
    return batch_x, batch_y.reshape(-1,1).astype(float)




if __name__ == '__main__':
	print('all_subjects:',len(SPLITS.all_subjects) , SPLITS.all_subjects)
	print('train_subjects:', len(SPLITS.train_subjects), SPLITS.train_subjects)
	print('test_subjects:', len(SPLITS.test_subjects) ,SPLITS.test_subjects)

	print('\n')

	print('all_views:', SPLITS.all_views)
	print('train_views:', SPLITS.train_views)
	print('test_views:', SPLITS.test_views)

	handler = TH.TableHandler()
	train_table = handler.tables.root.splits.WTrain1P
	
	tig = Input(train_table, 256)



