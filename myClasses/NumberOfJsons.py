import os
import numpy as np
import math
import myTime as T

JSONSPATH = 'C:/Users/up201/Desktop/Tese/openposeOriginal/openpose-master/examples/media/json3/'

def parseVideoName(name, verbose = 0):
	# Input : Video_name  Verbose
	# Output: Tupe(Setup, Camera, Subject, Replication, Action, Rest of the name as String )
	s = int(name[1:4])
	c= int(name[5:8])
	su= int(name[9:12])
	r= int(name[13:16])
	a= int(name[17:20])
	rest = name[20:]

	if verbose:
		print(f'Name: {name}\tSet: {s}, Cam: {c}, Sub: {su}, Rep: {r}, Act: {a}, Rest: {rest} ')
		input()
	return s,c,su,r,a, rest


class NumberOfJsons:
	def __init__(self, path = JSONSPATH):
		self.path = path
		self.setups = 17
		self.cameras = 3
		self.subjects = 40
		self.replications = 2
		self.actions = 60
		self.videos_json = np.zeros((self.setups, self.cameras, self.subjects, self.replications, self.actions))

		if not os.path.isdir(self.path):
			raise Exception(f'{self.path} doesn\'t exist')

		# Variables Flags for prints
		oldS, oldC, oldSu = 0, 0, 0
		
		# Variables Timers for prints
		clock = T.Clock(4)
		for i in range(4):
			clock.startTimer(i)

		#start, startS, startC, startSu = t.time_ns(), t.time_ns(), t.time_ns(), t.time_ns()
		
		for videofolder in os.listdir(self.path):
			s,c,su,r,a, _ = parseVideoName(videofolder)
			self.videos_json[s-1,c-1,su-1,r-1,a-1] = len( os.listdir(os.path.join( self.path, videofolder )) )
		
			oldS, oldC, oldSu =	T.Prints(oldS, oldC, oldSu, s,c,su, clock)
			
		clock.stopTimer(0,'Total')