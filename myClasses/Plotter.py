import numpy as np
import sys
import math
from matplotlib import pyplot as plt


sys.path.append('C:/Users/up201/chem/myClasses/')
import None_Checker as NC
import tableHandler as tH 
import myTime as T
import Bar as B


def configureAxes(xlabel=None, ylabel=None):

	plt.xlabel( xlabel)
	plt.ylabel(ylabel)





class Plotter:
	def __init__(self,condition = None):
		if condition:
			self.checker = NC.None_Checker(condition)
		else:
			self.checker = NC.None_Checker()

	def subplot(self, windowed= 'setup', XX = 'action', colored=None, preConditions = [], GraphType = B, test = 61, midRetA = True , ylim = None):
		clock = T.Clock()
		Ns = []

		print(windowed, colored, XX)
		if colored:
			print('\t\tSubplot 3')
			self.subplot3(windowed, colored, XX,preConditions=preConditions , ylim = ylim)
		else:
			print('\t\tSubplot 2')
			self.subplot2(windowed, XX,preConditions=preConditions, ylim = ylim)

		
	def plotHeatmap(self, fargs, title = None, savepath = None):
		heatMap = B.HeatMap(title,savepath)
		fargs, kwargs=heatMap.parseSP_args(*fargs)
		return heatMap.plot(*fargs, **kwargs)


	def plot(self, XX = 'setup', preConditions = [], midRetA = True ):
		clock = T.Clock()
		Conditions = preConditions 					
			
		XX_distrib = self.checker.countCol(XX, Conditions,returnAll = midRetA )
		print(XX_distrib)
		B.Bar(XX).plotBar(XX_distrib)


	def subplot3(self,windowed, colored, XX, preConditions = [], windowed_Conditions = [], GraphType = B, test = 61, midRetA = True , ylim = None):


		clock = T.Clock()
		Ns = []
		plotfunc = 'StackedBars'
		#print('ylim',ylim)
		windowDistrib = self.checker.countCol(windowed, windowed_Conditions)
		
		for n in range(windowDistrib.shape[0]):
			Conditions = preConditions + [f'{windowed} == {windowDistrib[n,0].astype(int)}']					
			
			XX_distrib = self.checker.countCol(XX, Conditions,returnAll = midRetA )
			if XX_distrib.shape[0]:
				Ns += [n]
		
			print(XX_distrib.shape)
			#print(colored_distrib)
		
		
		subplot_cols =  5
		if windowDistrib.shape[0]<5:
			subplot_cols = windowDistrib.shape[0]
		subplot_rows = math.ceil(len(Ns)/5)
		subplot_cols +=1

		print(f'Suplot shape = ({subplot_rows},{subplot_cols})\tWindow Filter shape = {windowDistrib.shape}, Test condition: n<{test}, {len(self.checker.None_location)}')
		print(windowDistrib)
		input()
		total = clock.startTimer()
		window = clock.startTimer()
		sp_i = 1

		for n in range(windowDistrib.shape[0]):
			if (n<test) & (n in Ns):

				Conditions = preConditions + [f'({windowed} == {windowDistrib[n,0].astype(int)})']
				
				colored_distrib = self.checker.countCol(colored, Conditions, returnAll = midRetA )
				XX_distrib = self.checker.countCol(XX, Conditions,returnAll = midRetA )

				#print(f'\n\tSubplot number  {n+1}: \tNumber of subplots = {windowDistrib.shape[0]}\t Ncolors = {colored_distrib.shape[0]}\t\t\t\t\tConditions: {Conditions}')
				
				if (n+sp_i)%subplot_cols == 0 :
						sp_i+=1

				if colored_distrib.shape[0]:
					plt.subplot(subplot_rows,subplot_cols, n+sp_i)

					title = f'{windowed} {windowDistrib[n,0].astype(int)}'
				
					miniPloter = GraphType.StackedBars(title = title)
					
					kwargs = {'preConditions': Conditions, 'XX' : XX,  'show':False}

					#print(n, colored_distrib.shape, windowed, title)

					fargs, kwargs =miniPloter.parseSP_args( self.checker, colored, colored_distrib , **kwargs)
					miniPloter.plot(*fargs, **kwargs)
					
					if ylim:
						ylim = 60#np.max(windowDistrib[:,1])
						#print('ylim=', ylim)
						plt.gca().set_ylim(0,ylim)


				clock.startTimer(clock.stopTimer(window,f'\t\tSubplot: {windowed} {windowDistrib[n,0].astype(int)} Duration',verbose=True))

		clock.stopTimer(total, 'Total', verbose = True)
		
		xlabel = XX.capitalize()+'s'
		ylabel='Number of None samples'

		fig = plt.gcf()
		fig.text(0.5, 0.04, xlabel, ha='center')
		fig.text(0.02, 0.5, ylabel, va='center', rotation='vertical')

		a=plt.subplot(1,subplot_cols, subplot_cols)
		a.axis('off')
		
		xi,yi,xd, yd =0.8,-0.2,0.19,1
		handles = []
		labels =[]
		for AX in plt.gcf().axes:
			handles_, labels_ = AX.get_legend_handles_labels()

			#print(AX.get_position())
			handles +=handles_
			labels += labels_

		by_label = dict(zip(labels, handles))
		print(handles)
		plt.gcf().legend(by_label.values(), by_label.keys(), bbox_to_anchor=(xi,yi,xd,yd), loc="upper left",labelspacing=0.05,
                borderaxespad=0, mode='expand', ncol = 2)

		plt.subplots_adjust(hspace = (0.4/4) * subplot_rows)
		print(f'NonesEach{windowed.capitalize()}Each{colored.capitalize()}Per{XX.capitalize()}')

		plt.show()

	
	def subplot2(self, windowed, XX='action', preConditions = [], GraphType = B,plotfunc = 'Bars',  test = 61, midRetA = True , ylim = None):
		clock = T.Clock()
		Ns = []
		plotfunc = 'Bars'
		print(windowed)
		windowDistrib = self.checker.countCol(windowed, preConditions)
		
		for n in range(windowDistrib.shape[0]):
			
			Conditions = preConditions + [f'{windowed} == {windowDistrib[n,0].astype(int)}']
				
			XX_distrib = self.checker.countCol(XX, Conditions, returnAll = midRetA )
		
			if XX_distrib.shape[0]:
				Ns += [n]
		
			print(XX_distrib.shape, Conditions, XX, midRetA)
		

		
		subplot_cols =  5
		if windowDistrib.shape[0]<5:
			subplot_cols = windowDistrib.shape[0]
		subplot_rows = math.ceil(len(Ns)/5)
		
		print(f'Suplot shape = ({subplot_rows},{subplot_cols})\tWindow Filter shape = {windowDistrib.shape}, Test condition: n<{test}, {len(self.checker.None_location)}')
		print(windowDistrib)
		input()
		total = clock.startTimer()
		window = clock.startTimer()
		sp_i = 1
		for n in range(windowDistrib.shape[0]):
			if (n<test) & (n in Ns):
				
				Conditions = preConditions + [f'{windowed} == {windowDistrib[n,0].astype(int)}']
				
				XX_distrib = self.checker.countCol(XX, Conditions, returnAll = midRetA )

				print(f'\n\tSubplot number  {n+1}: \tNumber of subplots = {windowDistrib.shape[0]}\t {XX} bars = {XX_distrib.shape[0]}\t\t\t\t\tConditions: {Conditions}')
				

				if XX_distrib.shape[0]:
					plt.subplot(subplot_rows,subplot_cols, n+sp_i)

					title = f'{windowed} {windowDistrib[n,0].astype(int)}'
					
				
					miniPloter = GraphType.Bars(title = title)
					
					kwargs = {'preConditions': Conditions, 'XX' : XX,  'show':False}
				
					kwargs = {**kwargs,'n': windowDistrib[n,0].astype(int)-1}#f'{windowed} {windowDistrib[n,0].astype(int)}'}
					#print(n, XX_distrib.shape,  title)
					fargs, kwargs =miniPloter.parseSP_args( self.checker, windowed, XX_distrib , **kwargs)
					miniPloter.plot(*fargs, **kwargs)
					
					if ylim:
						ylim = np.max(windowDistrib[:,1])
						#print('ylim=', ylim)
						plt.gca().set_ylim(0,ylim)


				clock.startTimer(clock.stopTimer(window,f'\t\tSubplot: {windowed} {windowDistrib[n,0].astype(int)} Duration',verbose=True))

		clock.stopTimer(total, 'Total', verbose = True)
		
		xlabel = XX.capitalize()+'s'
		ylabel='Number of None samples'

		fig = plt.gcf()
		fig.text(0.5, 0.04, xlabel, ha='center')
		fig.text(0.02, 0.5, ylabel, va='center', rotation='vertical')

		plt.subplots_adjust(hspace = (0.4/4) * subplot_rows)
		
		print(f'NonesEach{windowed.capitalize()}Per{XX.capitalize()}')



		plt.show()

	def plotAccLoss(self, history_path = 'Models/', modelname = 'model' ):
		hist_path = history_path + modelname + '_history.csv'
		values = np.genfromtxt(hist_path, delimiter= ',', skip_header = 1)

		fig = plt.figure()
		ax = fig.add_subplot(121)
		ax.set_title('Acc')
		ax.plot(np.arange(len(values)), values[:,2]*100, label = 'Train')
		ax.plot(np.arange(len(values)), values[:,4]*100, label = 'Test')
		ax.set_ylabel('Percentage')

		ax = fig.add_subplot(122)
		ax.set_title('Loss')
		ax.plot(np.arange(len(values)), values[:,1], label = 'Train_Loss')
		ax.plot(np.arange(len(values)), values[:,3], label = 'Test_Loss')
		plt.suptitle(modelname)
		plt.legend()
		plt.show()





	





