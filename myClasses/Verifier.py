import numpy as np
import sys
sys.path.append('C:/Users/up201/chem/tests/OpenPose/')
import NumberOfVideos as NV

def verify(parameter, verifier, expected):
	a = 'Passed' if verifier == expected else 'Reproved'
	print(f'{parameter}:   \t|\t  {verifier}\t|\t   {expected}\t\t| {a}')

class GeneralVerifier():
	Expected = [60,2,40,3,17]
	labels = ['Action', 'Replication', 'Subject', 'Camera', 'Setup']

	def __init__(self, metaTable):
		actionVerifier       	= len(np.unique(metaTable.cols.action[:]))
		replicationVerifier		= len(np.unique(metaTable.cols.replication[:]))
		subjectVerifier			= len(np.unique(metaTable.cols.subject[:]))
		cameraVerifier			= len(np.unique(metaTable.cols.camera[:]))
		setupVerifier 			= len(np.unique(metaTable.cols.setup[:]))

		self.actual = [actionVerifier, replicationVerifier, subjectVerifier, cameraVerifier, setupVerifier]
	
	def print(self,):

		print('/'*64)
		print('Parameter:\t|\tactual\t|\texpected\t| Status')

		for lab,ver,exp in zip(self.labels, self.actual,self.Expected):
			verify(lab, ver, exp)

		print('/'*64)



class SetupVerifier():
	n_setups = 17
	labels = ['Action','Replication', 'Subject', 'Camera']

	def __init__(self,metaTable):
		nv = NV.NumberOfVideos()
		
		ExpectedActions = [60]*self.n_setups
		ExpectedReplications = [2]*self.n_setups
		ExpectedSubjects = nv.getSubjectsList()
		ExpectedSubjects = np.array([len(a) for a in ExpectedSubjects]).astype(int)
		ExpectedCameras = [3]*self.n_setups
		self.Expected = np.array([ExpectedActions, ExpectedReplications, ExpectedSubjects, ExpectedCameras])

		self.actual = np.zeros((4,self.n_setups)).astype(int)

		for n in range(self.n_setups):
			index = metaTable.get_where_list(f'setup == {n+1}')

			actionVerifier       	= len(np.unique(metaTable.cols.action[:][index]))
			replicationVerifier		= len(np.unique(metaTable.cols.replication[:][index]))
			subjectVerifier			= len(np.unique(metaTable.cols.subject[:][index]))
			cameraVerifier			= len(np.unique(metaTable.cols.camera[:][index]))

			self.actual[:,n] = np.array([actionVerifier, replicationVerifier, subjectVerifier, cameraVerifier]).astype(int)
			print(f'Setup {n+1} finished')
	
	def print(self,):
		print('/'*100)
		print('Setup\t|\tParameter:\t|\tactual\t|\texpected\t| Status')
		newLabel = []
		newLabels = [None]*4
		for r,parameter in enumerate(self.labels):
			for setup in range(self.n_setups):
				#print(newLabel[0])
				name = f'Setup {setup+1:02}\t|\t'+parameter
				newLabel +=  [name]
			newLabels[r] = newLabel
			newLabel = []

		newLabels = np.array(newLabels)

		for r in range(newLabels.shape[0]):
			for c in range(newLabels.shape[1]):
				l=newLabels[r,c]
				a=self.actual[r,c]
				e=self.Expected[r,c]

				verify(l,a,e)
		print('/'*100)

		

